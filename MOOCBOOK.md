Comprendre l'Open Source
========================
![Début](images/mooc-comprendreopensource%20-%20001.jpg) 

# Introduction

## 1.1. Présentation du Mooc et de BJE

### Présentation du MOOC

Le mouvement Open Source est aujourd’hui omniprésent. Il s’invite progressivement dans les produits que nous utilisons et change peu à peu notre quotidien, que ce soit comme organisation ou comme particulier. S’il pouvait auparavant déranger, l’Open Source suscite aujourd’hui un intérêt certain de la part des acteurs du marché qui y voient une nouvelle voie favorable à la création et à l’innovation.

En effet, tout le monde cherche à trouver la meilleure façon de tirer profit de cette dynamique collaborative tout en maintenant certains avantages économiques : c’est dans l’appréciation de l’effort individuel face à celui porté par l’ensemble -- au sens du  collectif -- que le curseur doit être positionné.

![objectifs](images/mooc-comprendreopensource%20-%20004.jpg) 

Les objectifs de ce MOOC/guide sont multiples, à l’image de l’Open source, et le plan qui suit essaie d'en représenter toute sa diversité :
* Nous commencerons ainsi par replacer l’**Open source dans son contexte historique** afin de donner toutes les clefs de compréhension autour de la naissance et de l’évolution de ce modèle.
* Une identification de ses **caractéristiques** sera ensuite réalisée avant d’exposer l’**encadrement juridique** des **réalités économiques** de l’Open source.
* Elle sera par la suite suivie d’une **présentation des bonnes pratiques** nécessaires à la pérennité et la prospérité des projets Open source, à la fois à l’échelle de l’individu prenant part au projet et à celle des organisations.
* Enfin, quelques dernières vidéos aborderont de manière transverse les **liens entre l’Open Source et les grandes transformations** que connaissent nos organisations.

Ce MOOC sera illustré par des exemples concrets, des chiffres et des témoignages. Des outils d’exploration (tableaux, cartes mentales, etc.) adaptés à vos besoins et directement utilisables dans vos métiers seront – autant que possible – mis à disposition en parallèle du MOOC. Les vidéos sont réunies en chapitres décrivant autant de facettes de l’Open Source que nécessaire. Ces mêmes chapitres, complémentaires entre eux, permettront également d'avoir un accès aux différentes thématiques indépendamment des chapitres précédents. 

À l’issue de ce MOOC, vous aurez une **meilleure connaissance de l’histoire et du rôle de l’Open Source dans notre société** ainsi qu'un premier éclairage des **subtilités économiques et juridiques** sous-jacentes. Vous serez en capacité de tirer le meilleur profit de l’usage de l’Open Source, que ce soit dans votre vie personnelle ou au sein de votre organisation.

S’il a vocation à couvrir largement les questions que vous seriez susceptible de vous poser concernant l’Open Source, ce MOOC n’est néanmoins qu’une première approche de la multitude de sujets qui pourront donner lieu, le cas échéant, à d’autres MOOC plus spécifiques.

### Benjamin Jean

![Benjamin](images/mooc-comprendreopensource%20-%20005.jpg) 

Ce Mooc est présenté par Benjamin Jean, juriste de formation et spécialisé en propriété intellectuelle. Il s'intéresse plus spécifiquement aux modèles de collaboration et nouveaux usages tels que l’Open Source, l’Open Data et l’Innovation Ouverte. Dans la vie de tous les jours, il cherche à rendre possible, juridiquement et économiquement, une pleine collaboration entre une diversité d’acteurs de sorte que le potentiel de l'Open Source, accru par le numérique, puisse pleinement se déployer.

Dans ce cadre, Benjamin Jean a créé fin 2011 le cabinet [Inno³](http://inno3.fr) afin d’accompagner les acteurs, grands et petits, publics et privés, dans leurs stratégies d’ouverture. Enfin, très impliqué dans les enjeux industriels associés à l’adoption de l’Open Source en tant que cofondateur de l’[European Open Source & Free Software Law Event](http://eolevent.eu) (EOLE) depuis 2008, il a également co-présidé l’Open Source Summit (OSS) en 2016. 

Il est, parallèlement, très engagé dans le tissu associatif qui entoure le logiciel libre, au travers notamment de [Framasoft](http://framasoft.org) et [Veni Vidi Libri](http://vvlibri.org). Cet engagement dépasse d'ailleurs le domaine du logiciel libre avec des projets tels qu’[« Open Law, Le Droit Ouvert »](http://openlaw.fr), association dont il possède la présidence et qui applique les principes de l'innovation Ouverte au monde du droit pour amener tous ses acteurs, publics et privés, à collaborer à grand renfort de logiciels libres, Open Source et Open Data.

### Bibliographie

![ressources](images/mooc-comprendreopensource%20-%20006.jpg)

Dans cette optique, il a rédigé l’ouvrage “Option Libre” (édité par Framabook), qui vise à vulgariser les questions liées aux licences libres. En parallèle, il se trouve être également l’auteur de nombreux articles et guides touchant aux aspects juridiques et managériaux de l’Open Source, de l’Open Data, de l’Open Hardware ou plus largement de l’Innovation Ouverte. Toutes ces ressources sont disponibles sous licences libres et ouvertes, et régulièrement présentées dans le cadre des inno’vents, événements récurrents qui réunissent tous les deux mois les experts d’une thématique touchant aux modèles ouverts.

Pour finir, il donne régulièrement des formations sur toutes ces thématiques et enseigne la propriété intellectuelle auprès de juristes, ingénieurs & élèves de grandes écoles.

### Licence du contenu

![creativecommons](images/mooc-comprendreopensource%20-%20007.jpg)

Dans l’esprit même de l’Open Source, et, car nous appliquons les principes que nous promouvons, ce MOOC est mis à disposition sous une licence Creative Commons, notion sur laquelle nous reviendrons, afin que chacun puisse réutiliser le contenu, se le réapproprier, et éventuellement l'améliorer pour l'insérer dans de nouvelles ressources.

La retranscription ainsi que les images de la présentation sont également mises à disposition de toutes et tous sur [Framagit](https://framagit.org/inno3/mooc-comprendre-OS).


Ressources :
* [Framagit](https://framagit.org/inno3/mooc-comprendre-OS)
* [Option Libre, du bon usage des licences libres, Framasoft, coll. « Framabook », Paris, 2011, 307 pages, librement téléchargeable ; version papier : 20 euros.](https://framabook.org/optionlibre-dubonusagedeslicenceslibres/)
* [Licence Creative Commons 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
* Voir les ressources sur le site d'[inno3](https://inno3.fr/ressources) [Framagit](https://framagit.org/inno3/)

## 1.2. L’Open Source, un phénomène sociétal majeur

![phénomènesociétalmajeur](images/mooc-comprendreopensource%20-%20010.jpg)

Dans ce chapitre nous allons aborder l'Open Source sous différents aspects : comme un phénomène sociétal majeur, multidimensionnel et comme un modèle au coeur d'une dynamique globale de partage.

L’Open Source se caractérise par des logiciels que vous utilisez peut-être régulièrement et sans savoir qu'il s'agit de logiciels libres. C'est par exemple le cas du navigateur [Firefox](https://www.mozilla.org), du logiciel multimédia [VLC](http://videolan.org), ou encore du logiciel de traitement de texte [LibreOffice](https://www.libreoffice.org/).

### Exemple d'usages au quotidien

![quotidien](images/mooc-comprendreopensource%20-%20014.jpg)

Ces logiciels Open source que vous utilisez au quotidien, mais dont vous n’avez pas conscience, sont nombreux. Ainsi, 27 % des sites internet sont créés grâce au [CMS](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu) [WordPress](https://wordpress.com/) et 8 % avec [Drupal](https://www.drupal.org/), dont le site de la Maison-Blanche !

Plus largement, l’infrastructure sous-jacente du réseau Internet s’est développée en très grande partie sur la base de logiciels Open Source.


De la même façon, toutes les « box » que vous utilisez quotidiennement pour avoir accès à internet chez vous ont leur logiciel basé sur le système d'exploitation Open Source Linux. Il en va de même pour tous les objets connectés, qu'il s'agisse des Télévisions, des montres, et ce sera de plus en plus le cas avec le développement de l'Internet et des objets.

Aujourd'hui les grandes entreprises emploient massivement ce modèle de logiciel, ayant progressivement compris les enjeux stratégiques d’une telle adoption. 

### Open Source et GAFA

![ressources](images/mooc-comprendreopensource%20-%20019.jpg)

Cela a notamment commencé par les géants du numérique : Google, Amazon, Facebook, Twitter, dont le coeur de métier est lié au logiciel. Google par exemple, possède sa propre plateforme Open Source sur laquelle il présente tous les bénéfices tirés de cette adoption. 

Il y présente également tous les projets auxquels il contribue dans l'optique de permettre à d'autres acteurs de la même taille ou beaucoup plus petits d'alimenter ou tirer profit de cette ouverture. 

### Open Source et verticale métier

![ressources](images/mooc-comprendreopensource%20-%20024.jpg)

Mais l’Open source touche également des entreprises dans des secteurs industriels, comme celui de l’automobile, à l’image de BMW ou Renault. Renault a en effet communiqué sur une voiture Open Source Hardware (domaine du matériel), les équipes de la marque française s'appuyant pour ce faire sur un projet antérieur appelé 'Open Source Vehicle' -- aujourd'hui [OpenMotors](https://www.openmotors.co/) -- pour lancer ce projet. 

L’Open Source est aussi là où on ne s’y attend pas forcément, comme dans le secteur bancaire où elle est très présente : Goldman Sachs ou la Société Générale utilisent des politiques affirmées en faveur de ce modèle. 

### Open Source et logiciels d'éditeurs bien connus

![ressources](images/mooc-comprendreopensource%20-%20028.jpg)

Aujourd’hui même les éditeurs de logiciels non libres s’appuient sur des logiciels libres pour créer leurs produits : Apple a par exemple basé Mac OSX sur de l’Open Source, et c’est également le cas des projets proposés par l’entreprise SAP.

Dans un monde de plus en plus numérique, le logiciel libre est omniprésent, répondant à de plus en plus d’objectifs différents, mais convergents. Ainsi et même lorsque les finalités ne sont pas les mêmes, les moyens mis en place sont dans l’intérêt de tous les acteurs en présence qui acceptent l’Open source comme une démarche nécessaire. 

**Ressources** :
* [Histoires et cultures du Libre. Des logiciels partagés aux licences échangées](https://framabook.org/histoiresetculturesdulibre/)
* [Google Open Source](https://opensource.google.com/)
* [Communication autour du projet POM par Renault lors du CES](https://www.openmotors.co/renaultpomsignup/)

## 1.3. L’Open Source, un phénomène multidimensionnel

![multidimensionnel](images/mooc-comprendreopensource%20-%20029.jpg)

Qu’est ce que l’Open Source ? 

La désignation open source, ou « code source ouvert », s'applique aux logiciels dont la licence autorise la libre redistribution du logiciel, l'accès au code source et la création de travaux dérivés. 

### Les autres dimensions

![partage](images/mooc-comprendreopensource%20-%20031.jpg)

Elle recouvre néanmoins d’autres réalités. Mis à la disposition du grand public, ce code source est généralement le résultat d'une collaboration entre programmeurs et toute une communauté. 

Cette démarche matérialise des valeurs éthiques puisque l’idée sous-jacente est de permettre à l’utilisateur de garder le contrôle sur sa machine plutôt que d’être contrôlé par celle-ci. Dans le contexte actuel, où le numérique prend une part croissante dans nos vies, ces valeurs trouvent un écho particulier puisque tout ce qui peut aider les individus à garder une souveraineté a son importance.

![valeurs](images/mooc-comprendreopensource%20-%20032.jpg)

Il faut bien voir qu’entre valeurs éthiques et efficacité pragmatique, il n’y a pas d’opposition, mais au contraire une complémentarité. Pendant des années, les mouvements du logiciel Libre et de l’Open Source, respectivement portés par la Free Software Foundation et l’Open Source Initiative, se sont frontalement opposés. 

![fsf](images/mooc-comprendreopensource%20-%20033.jpg)

De fait, ils véhiculaient des idées ayant des finalités différentes : liberté et souveraineté de l’utilisateur pour le premier, agilité et mutualisation dans le développement pour le second au travers d’un processus de normalisation des licences Open Source. 

Complémentaires, les deux courants se rejoignent désormais et on considère leurs définitions comme équivalentes, même si elles proviennent de deux approches différentes.

À la base, l’Open Source se définit sur le plan juridique : il s’agit d’un logiciel que vous pouvez utiliser, modifier et rediffuser librement. Cette dimension fera l'objet d'une thématique dédiée dans ce MOOC. 

Mais cet aspect juridique a des conséquences organisationnelles fondamentales : le fait que la modification du logiciel, donc son amélioration, ne soit pas réservée à un acteur unique ouvre la voie à des pratiques de développement collaboratives intéressantes, facilitant l’agrégation d’efforts d’entités distinctes. Ces méthodes se sont révélées particulièrement efficaces, par exemple dans le cas du noyau Linux. Elles reposent notamment sur une transparence dans les processus, les outils et la communication permettant le même accès à l’information et plus d’interaction entre les intervenants. 

![essai](images/mooc-comprendreopensource%20-%20036.jpg)

L’essai « The Cathedral and the Bazaar » d'Eric Steven Raymond expose cette vision avec une clairvoyance certaine, qui anticipe les usages et méthodes les plus plébiscités aujourd’hui : agilité, lean management, etc.

Par ailleurs, ces dimensions juridiques et organisationnelles introduisent aussi des modèles économiques particuliers. 

![valeurs](images/mooc-comprendreopensource%20-%20037.jpg)

La libre diffusion bouleverse le marché, gouverné par des modèles cloisonnés dans lesquels chaque étape de la vie d’un produit signifie une nouvelle facturation. Libre n’est pas synonyme de gratuité, et nous reviendrons plus tard sur ce point, mais dans bien des cas les logiciels libres sont aussi disponibles gratuitement, ce qui leur permet de se diffuser et de toucher un public très large.  

On est donc sur des équilibres qui sont en train de se construire dans un contexte global, lui-même en pleine évolution, d'où l'intérêt de ce Mooc pour vous offrir toutes les clés de compréhension. 

**Ressources** :
* [The Cathedral and the Bazaar » d'Eric Steven Raymond](https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar)
* [Site de la Free Software Foundation](https://www.fsf.org/fr)
* [Site de l'Open Source Initiative](https://opensource.org/)

## 1.4. L’Open Source au coeur d’une dynamique globale du partage

![dynamique](images/mooc-comprendreopensource%20-%20039.jpg)

Ce n’est pas un hasard si le documentaire [Nom de code : Linux](https://fr.wikipedia.org/wiki/Nom_de_code_:_Linux), se termine par l’affirmation : « Ce serait peut-être l’une des plus grandes opportunités manquées de notre époque si le logiciel libre ne libérait rien d’autre que du code ». 

En effet, si le logiciel, qui est né avec le numérique, est le premier à avoir induit ces nouveaux usages et pratiques collaboratives, ces dernières ne sont pas restées cantonnées au logiciel et touchent aujourd'hui des domaines bien plus vastes.

En matière de contenu ouvert, les meilleurs exemples sont l’encyclopédie collaborative [Wikipédia](https://www.wikipedia.org/) et le stock de contenus ouverts disponible sur [Wikicommons](https://commons.wikimedia.org), véritablement devenu « mainstream » (autrement dit, un phénomène de masse). 

![phénomène](images/mooc-comprendreopensource%20-%20042.jpg)

Le phénomène de l’Open Data, qui consiste à mettre à disposition de tous des données ouvertes de manière similaire au logiciel Open Source, est une autre tendance majeure. Elle porte à la fois des enjeux de transparence au niveau des institutions publiques et des enjeux techniques et économiques forts, qui poussent de plus en plus d’acteurs privés à s’y intéresser. Le meilleur exemple représentatif de cette tendance est sans doute la loi pour une république numérique, récemment promulguée, mettant en avant ce caractère à la fois économique et démocratique de l'ouverture des données. 

Le projet [Open Street Map](https://www.openstreetmap.org/) est certainement le plus populaire des projets Open Data : véritable google Map ouvert et collaboratif, il cartographie le monde sous tous ses angles et est conçu de telle sorte à laisser à chacun libre cours à ses idées d’usage et de service comme la création de cartes ou de nouveaux produits ayant besoin de s'appuyer sur ces ressources. Ce projet est alimenté quotidiennement par des milliers, voire des millions de personnes à travers le monde. 

Ouvert et collaboratif, il s’avère déterminant dans des circonstances particulières comme les catastrophes naturelles et ne souffre pas de quelconque censure géopolitique.

![opengov](images/mooc-comprendreopensource%20-%20043.jpg)

On peut retrouver les différents projets Open Data sur des plateformes dédiées comme Data.gouv.fr, ou Opendatafrance.net. 
Dans la même dynamique, l’Open Gov est un mouvement visant à transformer le fonctionnement de notre démocratie grâce au numérique, en promouvant plus de transparence, d’ouverture et de collaboration dans les pratiques gouvernementales, au niveau national comme local et porté autant par les acteurs publics que la société civile. Dans ce cadre, de nombreux projets ouverts (Open Source et Open Data) ont émergé avec comme volonté de recréer du lien entre les citoyens et leurs représentants, et surtout, de rendre le citoyen acteur des décisions qui le concernent. 

![economiedupartage](images/mooc-comprendreopensource%20-%20044.jpg)

Si l’on se place à une échelle plus globale, on constate aisément qu’une économie du partage se développe et s’installe dans notre société depuis plusieurs années.

Elle recouvre des réalités différentes : mise en commun de biens, de services, de connaissances, entre particuliers, par le biais de plateformes numériques qui mettent en relation les parties prenantes de manière horizontale, de façon payante ou gratuite. Ces nouvelles formes de partage ont bouleversé des secteurs clés tels que le logement, le transport, la restauration, les services d’aide aux particuliers, etc.

Si l’économie sociale et solidaire va plus loin et ajoute une réelle couche éthique à ces échanges avec la primauté de l’intérêt général et l’organisation plus démocratique et transparente des relations, ce n’est pas nécessairement le cas des plus gros succès issus de ces courants (Uber, Airbnb) dont l’impact sociétal n’est pas conforme aux valeurs de partage initiales.

L’Open Source est un phénomène majeur qui a façonné de nombreux aspects du paysage technique, économique, mais aussi social actuel : il est important de pouvoir le replacer dans une perspective historique pour mieux en appréhender les mécanismes.

**Ressources** :
* [Voir le site data.gouv.fr](https://data.gouv.fr)
* [Benjamin Jean et Laure Kassem, Livre blanc "L'ouverture des données publiques : nouvelles obligations et nouveaux acteurs", 2016](https://inno3.fr/ressources/livre-blanc-louverture-des-donnees-publiques-nouvelles-obligations-et-nouveaux-acteurs)
* [Nom de code : Linux (vidéo)](https://www.youtube.com/watch?v=ANA134vEhEI)
* [Nicolas Colin et Henri Verdier, L'âge de la multitude, Entreprendre et gouverner après la révolution numérique, Armand Colin, 2015](http://www.armand-colin.com/lage-de-la-multitude-2e-ed-entreprendre-et-gouverner-apres-la-revolution-numerique-9782200601447)


# 2. Les grandes vagues de l’histoire du logiciel libre et Open Source

## 2.1. Les débuts de l'informatique : l'ère des hackers

![débutinformatique](images/mooc-comprendreopensource%20-%20048.jpg)

De la création des ordinateurs jusque dans les années 1970, les utilisateurs partageaient leurs programmes et contribuaient sur ceux de leurs pairs. De par leur prix et leur poids, l’accès des ordinateurs était restreint. À cela s’ajoutait le fait que l’usage de l’informatique requérait un certain niveau technique en la matière, il n’y avait pas de « simples utilisateurs » comme de nos jours.

![apparition](images/mooc-comprendreopensource%20-%20049.jpg)

Le partage de logiciels pour une relecture par leurs pairs était donc la solution logique utilisée par les chercheurs : les logiciels étaient à ce titre libres d'être étudiés, utilisés, modifiés, suivant ainsi la logique des sciences en général.

Au début de l’informatique, les logiciels étaient ainsi développés collectivement par un nombre restreint de personnes issues de la Recherche, de la Défense et des quelques entreprises vendant du matériel informatique.  

Elles avaient tout intérêt à permettre la création de communautés de développeurs pour favoriser l'évolution du logiciel, améliorant ainsi la qualité globale des services proposés par l’ordinateur qui était alors la propriété de ces contributeurs et répondait à leur volonté de maîtriser entièrement l’outil acquis.

![hackers](images/mooc-comprendreopensource%20-%20051.jpg)

C’est durant cette période que les premiers hackers se sont révélés. Contrairement aux idées reçues, hacker ne signifie pas « pirate informatique », mais au contraire un expert qui se délecte de la compréhension approfondie du fonctionnement interne d'un système, en particulier des ordinateurs et réseaux informatiques. Ils complétaient ainsi d’une certaine manière la recherche académique au travers d’une recherche informelle très finalisée et opérationnelle. Pour finir sur le sujet précisons qu’il s’agissait bien souvent des meilleurs programmeurs qui, dédaignant ce dernier terme, 
lui préféraient celui, plus argotique, d’hacker (bidouilleur).

![coûtinformatique](images/mooc-comprendreopensource%20-%20052.jpg)

Compte tenu du coût de fabrication des ordinateurs de l’époque et de la complexité en comparaison du coût relativement faible des logiciels, le prix de ces ordinateurs couvrait notamment les programmes destinés à être exécutés sur quelques-uns d'entre eux seulement.

Les logiciels étaient alors dénués de toute valeur économique intrinsèque, il n’y avait pas d’intérêt à les rendre rivaux ou exclusifs. Les logiciels étaient donc partagés aussi largement que possible au sein d'un écosystème de personnes intéressées par leur évolution.

Néanmoins, la baisse du coût des ordinateurs et l’augmentation de la valeur des logiciels, de plus en plus complexes et par ailleurs capables de fonctionner sur un environnement hétérogène (autrement dit, sur plus d'un ordinateur), ont graduellement renversé la tendance.

![democratisationlogiciel](images/mooc-comprendreopensource%20-%20054.jpg)

Avant les années 70, aucune propriété n’était, pour ainsi dire, revendiquée sur les logiciels. Néanmoins, la démocratisation de l’informatique va changer la donne, et favoriser le développement d’un marché du logiciel. Peu à peu ces produits immatériels vont se transformer en produits de consommation, à l’instar des livres ou des disques, par la reconnaissance d’un droit de propriété « intellectuelle » (en l’occurrence le droit d’auteur).  

![années7080](images/mooc-comprendreopensource%20-%20055.jpg)

Le logiciel fait alors l’objet d’un réel droit de propriété et sera vendu à l’unité pour un usage très limité dès les années 1980. En conséquence, les utilisateurs ne sont plus en droit d’utiliser les logiciels comme ils le souhaitent et doivent accepter de se conformer aux licences associées aux logiciels et définies par leur constructeur. 1980 fut l’âge d’or des éditeurs de logiciels (qui démocratisèrent le "shrink wrap", contrat associé à des logiciels vendus « sur étagère », et accepté par l’ouverture du produit), sur lequel reposent toujours des sociétés telles que Microsoft, Oracle, SAP, etc.

Ajoutons que parallèlement au développement de cette industrie (intimement liée à l’extension des droits qu’elle entretient par un lobbying intensif), certains logiciels continuèrent néanmoins à être créés collectivement, toujours au sein de la recherche (principalement au sein de l’Artificial Intelligence Lab du MIT, dit l’”AI Lab”), mais aussi parmi une communauté croissante de hackers. Ces derniers, n’ayant pas les  moyens ou ne souhaitant pas acheter de telles licences (alors généralement négociées par et pour les seules universités), mirent en place des règles de non-appropriation (non exclusivité) au travers des licences libres, rétablissant ainsi une confiance nécessaire au développement d’un projet commun.


**Ressources** :
* [Histoires et cultures du Libre. Des logiciels partagés aux licences échangées](https://framabook.org/histoiresetculturesdulibre/) 

## 2.2. La formalisation du logiciel Libre : RMS, FSF / GNU

<!---
![formalisationlogiciellibre](images/mooc-comprendreopensource%20-%20057.jpg)
-->

La mise en commun des compétences des hackers au sein du MIT et son fonctionnement horizontal (tous les hackers étaient placés au même niveau hiérarchique) a permis de créer des programmes robustes. Cette robustesse s’explique par le fait que chaque programme était le fruit de la collaboration de l’ensemble des hackers de l’AI Lab, car ceux-ci considéraient que le logiciel (et ses correctifs) pouvait servir à tous. Dans ce cadre, le partage leur est apparu comme une évidence.

![stallman](images/mooc-comprendreopensource%20-%20058.jpg)

Parmi les hackers de ce lab, Richard Stallman était l’un des plus investis et engagés politiquement. Il puise son militantisme de sa propre expérience, et plus particulièrement de l’anecdote de “l’imprimante” désormais devenue célèbre, qui stigmatise les limites du système mis en place par les éditeurs de logiciels : il souhaitait en effet réparer une imprimante en panne, mais n’avait pu y parvenir, car en raison d’un accord de non-divulgation il n'avait pas l'autorisation d'accéder au code source du logiciel. 

Face à ce constat, il lance dans les années 80 le projet GNU (GNU’s NOT UNIX) dont l’objectif est de créer un système d’exploitation entièrement libre. 

« S’il avait refusé sa collaboration pour des raisons personnelles, j'en serais resté là (...). Ce qui rendait l’enjeu important était le caractère systématique et impersonnel de son refus, le fait qu’il s’était engagé d’avance à ne coopérer ni avec moi ni avec aucune autre personne ». Cette phrase est la réponse du collègue de Stallman l'ayant amené à proposer une alternative. 

![creationfsf](images/mooc-comprendreopensource%20-%20060.jpg)

Fort du succès de GNU Emacs (son premier loiciel), Stallman fonda en octobre 1985 la Free Software Foundation (FSF), organisation américaine à but non lucratif, pour aider au financement du projet GNU et de la communauté du logiciel libre. 

C’est dans ce cadre que fut rédigée la Free Software Definition en 1986. Celle-ci définit comme libre tout logiciel pour lequel l’utilisateur bénéficie de trois libertés :

![liberte1](images/mooc-comprendreopensource%20-%20062.jpg)

La liberté d’étudier le fonctionnement du programme, et de l’adapter à ses besoins (l’accès au code source est donc nécessaire) (liberté 1) ;

![liberte2](images/mooc-comprendreopensource%20-%20063.jpg)

La liberté de redistribuer des copies du logiciel (liberté 2) ;

![liberte3](images/mooc-comprendreopensource%20-%20064.jpg)

La liberté d’améliorer le programme et de publier ses améliorations (là encore, l’accès au code source est nécessaire) (liberté 3).

![liberte0](images/mooc-comprendreopensource%20-%20065.jpg)

La liberté 0, celle d’exécuter le programme pour tous les usages, n’est apparue que plus tardivement aux alentours des années 1996, lorsque Stallman s’aperçut qu’elle pouvait ne pas être déduite des trois autres.

![typelicence](images/mooc-comprendreopensource%20-%20066.jpg)

On oppose aujourd’hui deux types de licence : les licences copyleft et les licences permissives. 

Les licences permissives assurent ces libertés évoquées précédemment pour les premiers utilisateurs du logiciel seulement alors que les licences copyleft les rendent persistantes au profit de tous les utilisateurs (premiers et subséquents) du logiciel, faisant reposer cette charge sur les licenciés eux-mêmes (les astreignant par exemple au maintien de la même licence sur leurs créations dérivées).

Cette protection, le copyleft, découlait essentiellement de déboires que connut Stallman lorsqu’il souhaita réutiliser une version modifiée de son logiciel Emacs (Gosling Emacs) et qu’il se vit opposer le copyright de l’auteur de cette dernière, situation qui le contraignit finalement à tout réécrire pour ne pas être inquiété par la société qui avait acheté les droits. Conditionnant la licence (au sens de permission) à une certaine réciprocité, il se servit ainsi de ses propres droits d’auteur pour limiter toute réappropriation par un autre. Ressentant comme une menace la combinaison de code libre et non libre, cette licence formalisa la notion de copyleft par laquelle l’ajout ou la jonction d’un élément quelconque à un programme sous une telle licence doit se faire de sorte que la version résultant de cette opération soit régie par les mêmes termes (et donc également libre).


**Ressources** :
* [Richard Stallman et la révolution du logiciel libre](https://framabook.org/richard-stallman-et-la-revolution-du-logiciel-libre-2/)
* [Free Software Definition](https://www.gnu.org/philosophy/free-sw.en.html)

## 2.3. La première vague d’adoption : Linus torvalds & Larry Wall

![vagueadoption](images/mooc-comprendreopensource%20-%20067.jpg)

Créée en 1989, la licence GNU GPL (General Public License) a progressivement été adoptée par des projets d’ampleur. 
Qu’il s’agisse de L. Peter Deutsch, de Larry Wall ou Linus Torvald, ces trois développeurs de renom étaient convaincus de l’intérêt du Libre pour leurs projets (respectivement Ghostscript, Perl et Linux) : en termes de méthode de développement, de diffusion et d'extension. 

![Ghostscript](images/mooc-comprendreopensource%20-%20069.jpg)

Approché et accompagné à ses débuts par Richard Stallman, L. Peter Deutsch avait logiquement choisi de diffuser Ghostscript sous GNU GPL dès son lancement dans les années 1988-1989. En revanche, pour ce qui est des projets de Linus Torvald et Larry Wall, l’adoption de la GNU GPL ne s’est faite qu'après une phase de maturation de ces dits projets.

![perl](images/mooc-comprendreopensource%20-%20070.jpg)

Pour le projet Perl, Larry Wall le publia en 1987 sous une licence spécifique interdisant l’usage commercial de son projet. Ce n’est qu’à la publication de sa 3e version, Perl 3.0 en 1989, que le logiciel fut soumis à la licence GNU GPL, faisant de ce projet l’un des premiers à être publié sous ce modèle en dehors des projets directement portés par Richard Stallman au sein de la FSF.

![linux](images/mooc-comprendreopensource%20-%20071.jpg)

Au départ, le noyau Linux lui-même n’était pas un logiciel libre. Linus Torvals mit en ligne le 25 août 1991 un message, désormais célèbre, annonçant qu’il avait créé un OS (Système d'exploitation) et qu’il désirait appeler à des contributions en diffusant le noyau Linux quelques semaines plus tard. À cette époque Linus Torvalds autorisait simplement la distribution gratuite et le report de bogues. Mais un peu plus d’un an plus tard, en février 1992 avec la montée en popularité du projet et sentant qu’il ne risquait plus de « perdre » sa création, Linus Torvalds rendit son logiciel libre et remplaça la licence spécifique obligeant la fourniture des sources et interdisant l’usage commercial par la licence GNU GPL de la FSF.

![3adoptions](images/mooc-comprendreopensource%20-%20072.jpg)

Ces trois adoptions constituèrent un signal fort : représentatif de l’intérêt que présentait la GNU GPL à l’égard des projets collaboratifs et renforçant celui-ci du fait de leur légitimité propre. Cependant, elles illustrent tout aussi bien les premières frictions face aux positions très fortes que traduisait cette licence tant d’un point de vue des particuliers que des industriels. 

Ainsi, Larry Wall et Linus Torvald ajoutèrent chacun une interprétation à la GNU GPL pour relativiser certains de ses effets en matière de combinaison de logiciels. Plus encore, Larry Wall rédigea en mars 1991 une nouvelle licence qu’il intitula Artistic License et L. Peter Deutsch créa en 1994 l’Aladdin Free Public License. 

L’Artistic License fut publiée à l’occasion de la sortie de la version 4 du langage Perl et s’ajouta à la licence GNU GPL (donc sous double licence) afin de « rassurer les industries du logiciel ». De son côté, l’Aladdin Free Public License fut utilisée par la société Aladdin Entreprise pour interdire certains usages commerciaux, en complément de la version GNU Ghostscript toujours diffusée sous licence GNU GPL. IL faut savoir qu'il y avait donc deux versions, la première entièrement GPL et communautaire, et la deuxième commerciale sous une licence permettant la rentabilité et d'avoir un véritable modèle économique autour d'un projet libre.

L'adoption de la GNU GPL par ces trois projets majeurs caractérisent parfaitement les moyens, les ouvertures et les opportunités que représente l'Open Source dans le cadre de modèles économiques et dans le développement de projets collaboratifs. 

**Ressources** :
* [L’évolution des licences libres et open source : critères, finalités et complétude ?](https://inno3.fr/sites/default/files/2017-05/Evolutiondes%20licences%20libres%20et%20Open%20Source.pdf)
* * [Option Libre, du bon usage des licences libres, Framasoft, coll. « Framabook », Paris, 2011, 307 pages, librement téléchargeable ; version papier : 20 euros.](https://framabook.org/optionlibre-dubonusagedeslicenceslibres/)
* [Pages « Comprendre les licences libres » sur Veni, Vidi, Libri](https://vvlibri.org/fr/comprendre-les-licences-libres)

## 2.4. L’appropriation croissante des industriels : de l’opportunisme à la mutualisation raisonnée

![industriel](images/mooc-comprendreopensource%20-%20073.jpg)

Dans un premier temps toutes les organisations voulaient “leur” Open Source, favorisant une multiplication des licences, des méthodes et des outils. Ainsi et en dépit d’un discours de partage et de collaboration, la mutualisation était très réduite et il était devenu particulièrement complexe et lourd pour un contributeur de passer d’un projet à un autre (et souvent impossible d’un point de vue du code). 

![coopération](images/mooc-comprendreopensource%20-%20075.jpg)

Heureusement, les organisations ont assez rapidement compris leur intérêt à chercher à travailler collectivement là où leur vocation n’était pas de faire cavalier seul : entre partenaires dans un premier temps, mais très rapidement entre clients et fournisseurs puis entre concurrents.

La collaboration entre concurrents, souvent désignée sous le terme de « coopétition » (contraction des termes « coopération » et « compétition »), est ainsi une dynamique récurrente dans le domaine de l’Open Source. C’est une dynamique fortement poussée par les géants de l’internet tels que Facebook, Google ou encore Twitter, dans le domaine du logiciel, des algorithmes et du matériel.

Tous confondus, ces acteurs admettent aujourd’hui tirer davantage bénéfice de l’Open Source lorsqu’ils permettent le développement communautaire d’un projet. Cela leur permet à terme d’adopter les ressources générées par cet écosystème, contrairement à un mode pyramidal classique dans lequel toute une partie de ces acteurs serait passive.

![genivi](images/mooc-comprendreopensource%20-%20076.jpg)

Le projet Genivi et le système d’exploitation Android sont nés d’une telle stratégie.

S’agissant de [Genivi](https://www.genivi.org/), un groupement de constructeurs concurrents est à l’origine de l’initiative et permet de produire les ressources dans le domaine de l'automobile. [Android](https://www.android.coma) quant à lui est né de la constitution d’une alliance entre divers constructeurs d’ordiphones, opérateurs et distributeurs et cet écosystème a d'ailleurs été un élément clé de son succès.

![postgresql](images/mooc-comprendreopensource%20-%20077.jpg)

La mutualisation opérée par l’entreprise de l’association [Postgre Fr](http://postgres.fr/) est un autre bel exemple de collaboration vertueuse : plusieurs grands groupes (Air France, EDF, Leclerc, Météo France, SNCF ou encore la Société Générale) ont accepté l’idée de contribuer au projet Open Source en finançant alternativement des développements réalisés par des contributeurs au coeur de la solution. 

![softwareheritage](images/mooc-comprendreopensource%20-%20078.jpg)

À l’extrême, certaines ressources sont vues comme un socle technique nécessaire présentant un intérêt tel qu’elles sont cofinancées par le secteur public et privé. Le projet Software Heritage, destiné à collecter, préserver et partager tous les logiciels disponibles publiquement sous forme de code source, est un parfait exemple.

![openlaw](images/mooc-comprendreopensource%20-%20079.jpg)

Dans une moindre mesure, d’autres initiatives de la même veine pourraient être évoquées, telles que l'Intelligence artificielle au sein du projet Open Law dans le domaine du droit et les données de transport pour le projet de la Fabrique des mobilités. Ces deux initiatives ont en commun de favoriser l’émergence et la pérennisation de ressources communes numériques au sein d’une communauté (appelée "les communs") qui en assure la gouvernance et l’exploite dans sa propre activité.

Le développement de certaines ressources dans un mode ouvert apparaît aussi comme la meilleure façon de mutualiser les forces dans un marché souvent compétitif où l’innovation demande de plus en plus d’investissement.

**Ressources** :
* [Open Handwet Alliance](http://www.openhandsetalliance.com/)
* [Voir le Groupe de Travail Inter-Entreprises (PGGTIE) de l'association Postgre Fr](https://www.postgresql.fr/entreprises/accueil)

## 2.5. L’évolution du logiciel libre à l’ère du Cloud

![erecloud](images/mooc-comprendreopensource%20-%20081.jpg)

Le terme « cloud » (ou « cloud computing ») désigne généralement la livraison de données et/ou de services à la demande par Internet. 

Ainsi, le cloud computing peut être vu comme « une nouvelle façon de délivrer les ressources informatiques, et non une nouvelle technologie » (définition du NIST – National Institute of Standards and Technology).

![typecloud](images/mooc-comprendreopensource%20-%20083.jpg)

Il existe trois grands types de clouds qui varient en fonction du degré de responsabilité partagé entre l’entreprise et le fournisseur du service : 
* IaaS : Infrastructure as a Service,
* PaaS : Platform as a Service, 
* Saas : Software as a Service, 

![modalitécloud](images/mooc-comprendreopensource%20-%20084.jpg)

Chacune de ces modalités de cloud recouvre une réalité différente avec un contrôle plus ou moins fort de l'usager et de l'hébergeur du service. Le recours au cloud est aujourd’hui de plus en plus présent, notamment pour le SaaS, c’est-à-dire la fourniture de services logiciels en ligne.

Il s’agit de mettre à la disposition des consommateurs des applications auxquelles les utilisateurs sont généralement invités à accéder à partir d’un simple navigateur web ou de leur smartphone.

![responsabilites](images/mooc-comprendreopensource%20-%20085.jpg)

Ce type de service cloud a l’avantage d’être très facile à utiliser pour l’usager : les applications n’ont pas besoin d’être installées sur leur appareil, et n’ont pas non plus besoin d’être mises à jour, etc., car tout est fait chez le fournisseur du service.

Avec le SaaS, les utilisateurs n’installent plus le logiciel sur leur ordinateur, ils n’ont pas de fichier exécutable : celui-ci est situé sur un serveur à distance et ils y accèdent en ligne. Il n’y a donc plus réellement d’utilisation directe du logiciel au titre de la définition de la FSF.

![nitot](images/mooc-comprendreopensource%20-%20086.jpg)

Comme le rappelle Tristan Nitot, il convient donc d’être vigilant avec l’expression « informatique dans les nuages* »  (*Cloud Computing). Les données ne sont en réalité pas dans les nuages, mais sur les serveurs de quelqu’un d’autre. Cela revient à placer ses documents et/ou ses données sur les machines d’un prestataire, sans savoir où sont situées ces machines et s’il est possible de réellement faire confiance au prestataire en question.

![adaptationlicence](images/mooc-comprendreopensource%20-%20087.jpg)

Le recours au cloud computing déplaçant les enjeux au détriment des utilisateurs, Richard Stallman a émis de vives critiques à propos du SaaS, qu’il désigne par le terme « SaaSS » (service se substituant au logiciel).

L’utilisateur du service est mis à l’écart de l’accès au logiciel et n’a aucun contrôle possible sur celui-ci. Du fait que le logiciel ne soit pas situé sur l’ordinateur de l’utilisateur, mais sur un serveur distant, celui-ci ne possède aucun moyen d'accès à son code source. Il est donc impossible pour lui de vérifier ce qu’il fait vraiment, et de le modifier.

Cela a donc conduit au développement de nouvelles licences prenant en compte les spécificités du cloud. La licence la plus connue est probablement la GNU Affero GPL (AGPL), dérivée de la licence GNU GPL ; mais on peut également citer la licence EUPL rédigée par la Commission Européenne et qui ajoute cette spécificité depuis sa version 1.2.

**Ressources** :
* [Tristan Nitot, Numérique, reprendre le contrôle, ed. Framasoft, 2016](https://framabook.org/numerique-reprendre-le-controle/) avec :  Daniel Kaplan ; Adrienne Charmet ; Armony Altinier ; Isabelle Falque-Pierrotin ; Charles Schulz ; Fabrice Rochelandet ; Pierre-Yves Gosset ; Primavera De Filippi ; Michel Reymond ; Christophe Masutti ; Alain Damasio
* [Voir le programme Degooglisons Internet de Framasoft](https://degooglisons-internet.org)

# 3. L’assise juridique de l’Open Source

## 3.1. Comprendre le logiciel pour comprendre l’Open Source

![comprendrelogiciel](images/mooc-comprendreopensource%20-%20090.jpg)

Il est nécessaire de définir plusieurs notions afin de mieux comprendre les subtilités et attachements de l’Open Source : code source, code binaire, compilation, du SaaS, des API, tout autant de notions techniques, mais ayant une application concrète dans le domaine de l'Open Source. 

![defcodesource](images/mooc-comprendreopensource%20-%20091.jpg)

Le code source est un texte qui représente les instructions de programme telles qu'elles ont été écrites par un programmeur. Le code source est écrit dans un langage de programmation permettant d’être compris par les humains.

![binaire](images/mooc-comprendreopensource%20-%20092.jpg)

Une fois le code source écrit, il permet de générer une représentation binaire d'une séquence d'instructions — code binaire ou code objet — exécutables par un microprocesseur servant d'interface avec la machine. Ce langage binaire est uniquement compréhensible par l’ordinateur qui va l’utiliser pour exécuter le programme. Ce code est donc le résultat de la compilation du code source.

La compilation est ainsi l’opération réalisée par un programme (le compilateur) consistant à transformer le code source écrit dans un langage de programmation, en un autre langage informatique : le langage cible.

Pour qu’il puisse être exploité par la machine, le compilateur traduit le code source en un langage machine. De façon marginale, certains langages n’ont pas besoin de passer par une étape de compilation afin d’être exécutés (on dira alors qu’ils sont interprétés via des machines virtuelles).

![agrégation](images/mooc-comprendreopensource%20-%20093.jpg)

Un autre concept important à saisir concerne les liens dynamiques ou statiques qu'il peut y avoir entre les logiciels. Certaines licences libres et Open Source attachent une importance particulière aux relations qui lient chaque composant. 

La simple « agrégation » est ainsi souvent opposée à la « combinaison de deux modules dans un seul programme » :
* La notion d’agrégation concerne classiquement la situation où plusieurs programmes sont côte à côte sur un même support (tel qu’un DVD, Disque dur ou encore une clé USB).
* Dans un cas de simple agrégation, il s'agit de programmes distincts et non de parties d'un même programme. Dans ce cas, si l'un des programmes est régi par une licence, cela n'a pas d'effet sur l'autre programme. Ce point est d’ailleurs l’un des critères de l’Open Source Definition. En ce sens, une licence allant au-delà de la simple agrégation ne peut être considérée comme Open Source.

![liendynamiquestatique](images/mooc-comprendreopensource%20-%20094.jpg)

À l’inverse, si les modules sont inclus dans un même fichier exécutable, ils sont indéniablement combinés dans un seul programme. Il en va de même si les modules sont conçus pour être exécutés par édition de liens, cela signifie presque à coup sûr une combinaison en un seul programme.

Il existe ainsi deux types de liens, les liens dynamiques et les liens statiques. Entre une liaison dynamique et une liaison statique, la différence se situe lors de la sollicitation de la bibliothèque par le programme utilisateur : en cas de liaison statique, le programme sera complété au moment de la compilation pour ne faire qu'un seul binaire ; en cas de liaison dynamique, le programme sera complété au moment de l’utilisation.

![protectiontechnique](images/mooc-comprendreopensource%20-%20095.jpg)

Par ailleurs, la notion de DRM (Mesures Techniques de Protection) concerne les protections techniques susceptibles d’être intégrées à une oeuvre afin de limiter son utilisation à certains contextes. Par exemple, il peut s'agir de limiter le nombre d'utilisateur pour un logiciel donné sur certains types d'ordinateur, ou encore d'en limiter la durée d'utilisation. 

De la même façon que ce qui fut appelé plus tard la “Tivoization”, en référence à la société Tivo qui avait limité son matériel afin qu’il ne puisse exécuter des versions modifiées par des tiers de sa version de Linux, ces pratiques sont très contestées dans le domaine du logiciel libre qui cherche à ouvrir le plus de champs d’action possibles aux potentiels contributeurs et utilisateurs.

Toute solution technique qui viendrait limiter la capacité d'amélioration du logiciel serait considérée comme contraire à l'esprit de la licence.

![saasapi](images/mooc-comprendreopensource%20-%20096.jpg)

Pour finir, il est nécessaire de bien préciser les notions de distribution, d’usage par le réseau et d’API.
La notion de distribution correspond à la mise à disposition d’une copie de la création, logiciel ou autre, à un tiers. Elle peut être réalisée au travers d’un support matériel ou par le réseau, c'est-à-dire le transfert d'un fichier par internet.

![saas](images/mooc-comprendreopensource%20-%20098.jpg)

Le Saas, pour “Software as a Service” désigne l’utilisation de logiciels faite sur les serveurs d’un opérateur sans que les logiciels ne soient distribués aux utilisateurs de ce service.

Précisons néanmoins que certains langages s’exécutent du côté du client, c’est-à-dire de l’internaute, et que le chargement de la page internet aura pour première conséquence le téléchargement puis l’exécution dudit code, on parle alors de distribution classique.

![api](images/mooc-comprendreopensource%20-%20099.jpg)

Enfin, les API sont des interfaces de programmation, des passerelles permettant à deux applications autonomes de dialoguer et d'échanger au travers d’un réseau, au travers de protocoles standards. S’il est bien sûr nécessaire de se conformer aux conditions d’utilisation de ces APIs, cela n’entraîne aucune conséquence en termes de licences Open Source.

**Ressources** :
* [Page Wikipedia sur la Tivoisation](https://fr.wikipedia.org/wiki/Tivoisation)
* [Page Wikipedia sur les DRM](https://fr.wikipedia.org/wiki/Gestion_des_droits_num%C3%A9riques)
* [Site du projet Veni, Vidi, Libri](http://vvlibri.org)

## 3.2. Principes juridiques élémentaires de l’Open Source


![principesjuridiques](images/mooc-comprendreopensource%20-%20100.jpg) 

Tout logiciel est susceptible d’être protégé par des droits de propriété intellectuelle.

Les droits de propriété intellectuelle se décomposent en différentes branches : d’un côté la propriété littéraire et artistique permettant la constitution de monopoles d’exploitation dès la création de l’objet de la protection (ex : œuvre de l’esprit protégée par un droit d'auteur), de l’autre côté la propriété industrielle qui repose sur l’obtention de titres après avoir suivi une procédure de dépôt auprès de l’INPI.

![proprieteartistique](images/mooc-comprendreopensource%20-%20102.jpg)  

La propriété littéraire et artistique est composée:

* du **droit d’auteur** : l’auteur d’une œuvre de l’esprit (création de forme originale) dispose d’un monopole d’exploitation exclusif sur celle-ci. Il est titulaire de droits patrimoniaux (droit d’exploiter l’oeuvre) et d’un droit moral (droit au respect de son nom, sa qualité et son œuvre) ;
* des **droits voisins** : droit des artistes-interprètes, droit des producteurs de phonogrammes et de vidéogrammes. Ils ne sont pas ici détaillés, car ne concernent pas le logiciel
* du ***droit sui generis*** des bases de données : protège les investissements du producteur de la base de données qui a le droit d’interdire l’extraction de tout ou partie du contenu de la base.

![proprieteindustrielle](images/mooc-comprendreopensource%20-%20103.jpg)  

La propriété industrielle :

* le droit des **marques** : la marque est un titre de propriété portant sur un signe distinctif (ex : logo, nom). La durée de protection est de 10 ans (renouvelable indéfiniment) ;
* le droit des **brevets** : le brevet est un titre de propriété portant sur une invention. La durée de protection est de 20 ans à compter du jour du dépôt. Une fois que le brevet est arrivé à son terme, l’invention est considérée comme étant dans le domaine public ;
* le droit des **dessins et modèles** : protège l’apparence d’un produit ou d’une partie de produit (textures, contours, couleurs, matériaux, formes, lignes etc.) 
* On retrouve également d'autres droits non détaillés ici, car ne concernent pas le logiciel.

![droitdauteur](images/mooc-comprendreopensource%20-%20104.jpg)  

Dans ce cadre, le logiciel (entendu au sens strict : code source, code objet, documentation associée (cahier des charges, dossiers de spécifications fonctionnelles...) est protégé par le droit d’auteur. Toute reproduction, traduction, modification ou tout arrangement ne peut être réalisé sans l’accord préalable de l’auteur (sauf pour des raisons d’interopérabilité).

Le logiciel peut être commercialisé sous une marque, laquelle ne pourra pas être utilisée par un tiers dans la vie des affaires pour des produits identiques ou similaires, sans autorisation préalable du titulaire des droits. 

Le logiciel peut faire l’objet d’un brevet ou intégrer un, ou des composants faisant eux-mêmes l’objet d’un brevet. Il est alors nécessaire d’obtenir l’accord préalable du titulaire des droits. Cependant, l’Office européen des brevets refuse de délivrer des brevets sur des logiciels (sauf en tant que *business methods*), à la différence des Etats-Unis où c’est une pratique courante. 

A défaut d’autorisation, toute personne effectuant un acte relevant du monopole du titulaire des droits s’expose à une action en contrefaçon.

À ces droits de propriété intellectuelle s’ajoute la liberté contractuelle. Le droit des contrats (l’autorisation du titulaire des droits) se matérialise au travers de la conclusion d’un contrat de licence par lequel il concède un certain nombre de droits limitativement énumérés (tout ce qui n’a pas été concédé est considéré comme retenu). Ce contrat définit le périmètre des actes autorisés, toutes les actions se trouvant en dehors de ce périmètre sont considérées comme de la contrefaçon.

**Ressources** :
* Michel Vivant, Jean-Michel Bruguière, Droit d'auteur et droits voisins, Dalloz, Edition : 12/15 - 3e édition

## 3.3. Le droit utilisé comme interface pour favoriser la collaboration

![interfacecollaboration](images/mooc-comprendreopensource%20-%20107.jpg) 

Le droit apparaît souvent comme un domaine complexe, technique et peu accessible. 

Pour autant, le droit a pour vocation première d’organiser les relations sociales et ainsi de favoriser la collaboration. En proposant un cadre transparent et sécurisé, les outils juridiques renforcent la confiance des acteurs, leur permettant de coopérer efficacement.

Le droit est ainsi au cœur des projets Open Source, encadrant aussi bien la diffusion du logiciel que les relations entre les acteurs qui gravitent autour.

![licenceslibres](images/mooc-comprendreopensource%20-%20109.jpg)

L’exemple des licences libres et open source est le plus parlant.

Concrètement, la licence libre est le contrat qui relie les contributeurs d’une communauté. Elles bouleversent la propriété intellectuelle sans toutefois la renverser : s’adaptant aux nouveaux usages du numérique, la propriété intellectuelle est ainsi la base sur laquelle se construit la collaboration.

![absencelicencelibre](images/mooc-comprendreopensource%20-%20110.jpg)

Il faut bien comprendre que l'absence de licence Open Source associée à un projet empêche la réutilisation par les tiers et donc la collaboration.

Les licences ne sont ainsi et en réalité « que » des outils construits pour organiser un cadre de coopération entre tous les contributeurs et utilisateurs d'un projet. Pensées par le « bas », c’est-à-dire par les utilisateurs des logiciels, elles permettent d’organiser la collaboration entre plusieurs acteurs, parfois des milliers, qui partagent leurs droits sous la même licence.

Le cadre juridique qui porte le mouvement de l’Open Source ne peut que nous étonner par son succès et sa capacité à s’être imposé au fil du temps, en témoigne le nombre réduit de contentieux. Les licences libres rassurent, par les garanties qu’elles confèrent en matière de souplesse, de coût, de fiabilité, d’innovation, de pérennité.

![contributoragreement](images/mooc-comprendreopensource%20-%20112.jpg)

Au sein des projets ouverts, au-delà des licences libres, d’autres documents spécifiques jouent des rôles structurants et organisent la collaboration. 

Par exemple, de nombreuses communautés Open Source font signer aux contributeurs, à chaque contribution au projet, des documents spécifiques : les accords de contributions. Ces accords (CLA, CAA…) sont des textes qui encadrent juridiquement les contributions de tiers aux logiciels développés. 

En effet, lorsqu'un contributeur souhaite proposer des éléments soumis à des droits de propriété intellectuelle (code, documentation) pour que ceux-ci soient intégrés dans un projet Open Source, il est nécessaire qu'il place sa contribution sous la ou les licences du projet et donc qu'il dispose des droits de propriété intellectuelle.

Il est ainsi courant que les mainteneurs du projet lui demandent de signer un document spécifique en attestant, et délimitant précisément les droits cédés par le contributeur sur la contribution qu’il soumet. 

BREF : ces documents jouent un rôle déterminant pour instaurer et renforcer la confiance des parties prenantes d’un projet ouvert. 

**Ressources** :
* [Harmony Agreement](http://harmonyagreements.org/)
* [CLA Hub](https://www.clahub.com/pages/why_cla)
* [Site du projet Veni, Vidi, Libri](http://vvlibri.org)

## 3.4. Les licences libres

![sectionlicencelibre](images/mooc-comprendreopensource%20-%20113.jpg)

Une licence libre est un contrat par lequel l'auteur met à disposition son œuvre, il accorde une cession non exclusive de ses droits. Ainsi tous les réutilisateurs se voient accorder des libertés sur celle-ci via la licence.

![gnugpl](images/mooc-comprendreopensource%20-%20115.jpg)

Au départ il existait une seule licence par logiciel, et c'est uniquement en 1989, avec la GNU GPL, qu'on en a créé une pouvant être utilisé pour tout type de logiciel : c'est alors une rupture avec le mouvement précédent où l’on créait une licence par projet. 

La limitation du nombre de licences permet de rendre les projets interopérables, d’harmoniser les cadres juridiques pour faciliter la réutilisation des contenus licenciés.

![usagestandards](images/mooc-comprendreopensource%20-%20116.jpg)

L'usage de standards fluidifie les échanges et interconnexions, ainsi que la gestion des droits et des obligations liées aux licences. Les utilisateurs ont moins d’efforts à fournir pour utiliser les projets libres étant donné qu’il y a moins de licences différentes à gérer.

Depuis cette vague de standardisation, les licences sont de plus en plus génériques, permettant ainsi de lutter contre la prolifération de nouvelles.

![spdx](images/mooc-comprendreopensource%20-%20117.jpg)

Initié par les industriels, le projet SPDX (qui a vocation à favoriser l’échange d’informations relatives aux composants et aux licences Open Source) a permis de pousser encore plus loin ce mouvement de standardisation, en créant un identifiant unique pour chaque licence. 

![permissiveetcopyleft](images/mooc-comprendreopensource%20-%20118.jpg)

Comme évoqué précédemment, il existe aujourd'hui deux types de licence : les copyleft et les permissives. 

Les licences copyleft rendent persistantes les libertés consenties en obligeant les utilisateurs subséquents à concéder systématiquement les mêmes libertés. 

Il est parfois dit des licences copyleft qu’elles “forcent” à la liberté pour que ces contributeurs subséquents restent titulaires de leurs droits et donc libres d'exploiter leurs contributions au projet. Il en résulte une relation de confiance qui sécurise et favorise les collaborations entre professionnels et particuliers.

À l'inverse, on parle de licence permissive lorsque seules les obligations de celui qui reçoit l'œuvre doivent être transmises, laissant le contributeur libre d'en ajouter d'autres lors du transfert aux utilisateurs ultérieurs (le logiciel redistribué perd souvent les libertés qui lui étaient attachées).

Elles sont souvent assimilées à des renonciations de droits du fait de leur proximité avec le statut de domaine public puisqu'elles n'imposent en règle générale que le respect de la paternité, autrement dit il est demandé de citer le nom de l'auteur d'une oeuvre lorsque vous êtes utilisateur sous licence permissive.

Elles sont notamment compatibles avec une redistribution commerciale (cf. MIT, BSD, Apache) --> cf. MAC OS X

À noter que les licences ne sont pas figées, elles évoluent en fonction des besoins des contributeurs des projets, en général pour gagner en précision et en qualité, ou recouvrir d’autres aspects même si leur rythme d’adaptation est sensiblement plus lent que celui des projets Open Source eux-mêmes.


**Ressources** :
* [Option Libre, du bon usage des licences libres, Framasoft, coll. « Framabook », Paris, 2011, 307 pages, librement téléchargeable ; version papier : 20 euros.](https://framabook.org/optionlibre-dubonusagedeslicenceslibres/)
* [François Pellegrini et Sébastien Canevet, Droit des logiciels - Logiciels privatifs et logiciels libres,  PUF (20 novembre 2013)](https://www.lgdj.fr/droit-des-logiciels-9782130626152.html)
* [Site du projet Veni, Vidi, Libri](http://vvlibri.org)

## 3.5. Licences permissives : BSD, MIT et Apache

![licencepermissive](images/mooc-comprendreopensource%20-%20120.jpg)

Les licences permissives font partie des premières licences libres, issues du courant universitaire.

En raison de leur simplicité de prise en main, de nombreuses nouvelles licences permissives apparurent durant les années 1990. 

La plupart étaient des variations plus ou moins fidèles des licences publiées par le MIT et Berkeley, respectivement les licences MIT et BSD, mais d’autres se démarquèrent.

![apachegroup](images/mooc-comprendreopensource%20-%20122.jpg) 

L’Apache Group, à l’origine du serveur web éponyme, mit ses développements sous une licence très libérale de type «This code is in the public domain, and give us credit» et adopta en 1995 une première version de la licence Apache s’inspirant de la licence BSD originale, avant de lancer une seconde version en 2004 qui lui était propre.

Le respect de la paternité de l’oeuvre est souvent l’unique revendication des contributeurs passionnés, en règle générale les licences permissives imposent simplement la transmission de ces dernières et une mention des droits d’auteurs avec le logiciel.

La première version de la licence BSD (dite 4-clause BSD) contenait une clause qui devint rapidement polémique : tous les documents publicitaires mentionnant les caractéristiques ou l’utilisation de ce logiciel devaient afficher une mention concernant tous les auteurs. Parfois une centaine de mentions était obligatoire lors de la commercialisation d'un produit utilisant les composants sous ces licences permissives. 

Il s’en est suivi des situations où le formalisme imposé était beaucoup trop lourd pour être raisonnable étant donné le nombre important de contributeurs et de composants utilisés. 

Aujourd’hui les licences se sont adaptées pour éviter ces inconvénients, et ce type de clause reste marginale.

Un des objectifs des licences est d’être facilement utilisable pour permettre le partage de la connaissance, tout en étant un socle suffisamment contraignant pour pouvoir garantir le respect des droits des auteurs d’une part et la liberté des utilisateurs d’autre part.

Les licences permissives sont donc moins contraignantes que les licences de type copyleft. Le projet licencié et ses modifications intégrées à un projet plus vaste peuvent être distribués sous d’autres termes et sans le code source. 

![cohabitationlicences](images/mooc-comprendreopensource%20-%20123.jpg) 

Les licences permissives comme les licences MIT, BSD et Apache supportent très bien la cohabitation avec d’autres licences. Pour ces raisons, l’utilisation de composants, dans ce contexte, intégrés dans des projets commerciaux est courante.

La majorité des produits de la firme Google est distribuée sous licence permissive, Android est essentiellement distribué sous licence Apache, à l’exception de certains composants bien isolés tel le noyau Linux.

## 3.6. Le “copyleft” : la licence GNU GPL

![licencecopyleft](images/mooc-comprendreopensource%20-%20124.jpg) 

Licence emblématique, la licence GNU GPL est l’une des plus utilisées dans le monde. 

![licencegnugpl85](images/mooc-comprendreopensource%20-%20125.jpg) 

Richard Stallman, son auteur et également créateur du logiciel GNU Emacs, a décidé de lui associer en 1985 une licence spécifique, qui définissait avec précision le cadre permettant la réutilisation de la licence.

Surtout, elle imposait la conservation de la licence sur toutes les versions dérivées (ou contenant une partie) du logiciel. Il s’est ainsi servi de ses propres droits d’auteurs pour limiter toute réappropriation du logiciel par d’autres.

C’est la “naissance” du copyleft, share alike ou encore partage à l’identique.

De nombreuses licences copyleft furent ensuite rédigées par la FSF, en adaptant à chaque fois le texte aux logiciels ciblés, renforçant progressivement l’assise juridique des licences et garantissant plus de sécurité aux utilisateurs. 

![licencegnugpl89](images/mooc-comprendreopensource%20-%20126.jpg) 

En 1989 a été publiée la licence générique du projet GNU : la GNU General Public License (GNU GPL), répondant ainsi à une volonté d’harmonisation des textes, ce qui eut l’effet d’accélérer la diffusion de la licence, devenant rapidement centrale dans le paysage du Libre.

S’appuyant sur les nombreux retours de la communauté croissante des hackers ayant adopté cette licence, une seconde version de la GNU GPL fut publiée en 1991 (introduisant pour la première fois une clause relative aux brevets).  

Utilisée par de très nombreux projets open source, la licence GNU GPL a véritablement su s’imposer : les statistiques sur les licences la positionnent généralement en lice avec la licence MIT. Connue des développeurs, elle bénéficie d’une véritable communauté de confiance. Ce succès est l’aboutissement d’un processus continu d’adaptation aux évolutions des réalités économiques et juridiques des industries.

![licencegnugpl2007](images/mooc-comprendreopensource%20-%20127.jpg)

Ainsi, sa dernière version - GPL v3, sortit en 2007 - a fait l’objet de longs travaux collectifs lors d’une consultation publique ayant duré un an et demi.

Parmi les améliorations apportées par cette version, on peut citer une meilleure lisibilité de la licence et une protection renforcée contre les brevets logiciels. Autre nouveauté, la GPL v3 offre la possibilité à un tiers qui aurait violé la licence de revenir dans ses droits lorsqu’il cesse l’infraction.

Enfin, la GPL v3 bénéficie d’une meilleure compatibilité avec les autres licences libres, afin de permettre plus de coopération entre les projets Open Source.

Apparue la même année, la licence Affero GPL (identique à la GPL, à l’exception de son article 13) organise un mécanisme spécial permettant d’imposer au licencié de distribuer ses codes sources lorsqu’il modifie un composant soumis à cette licence et qu’il le fait interagir avec des tiers par le réseau.

## 3.7. Les « Weak Copyleft » : les licences GNU LGPL et MPL

![weakcopyleft](images/mooc-comprendreopensource%20-%20129.jpg)

À mi-chemin entre les licences à copyleft « fort » et les licences permissives se trouve un autre type de licence dite à copyleft « faible, réduit ou amoindri».

En pratique, lorsqu’une œuvre est placée sous un tel statut, la création originale et les modifications qui lui sont faites sont soumises à cette licence, mais elle peut néanmoins se combiner avec d’autres créations elles-mêmes sous d’autres licences.

Autrement dit, au sens strict du droit d’auteur, toutes les œuvres dérivées (pensez “transformées”) de l’œuvre originale seront soumises à la licence initiale, tandis que les œuvres composites (pensez “combinaison de différentes oeuvres") pourront être soumises à d’autres.

La conséquence immédiate est que certaines fonctionnalités ou composants vont pouvoir être ajoutés sous leur propre licence.

À savoir : les licences à « copyleft faible » les plus connues sont les licences LGPL et MPL.

![premiereversiongnugpl](images/mooc-comprendreopensource%20-%20131.jpg)

La première version de la licence GNU LGPL (GNU Library General Public License) a été publiée en 1991. Conçue pour être moins contraignante que la GPL, elle était destinée à certains programmes qui gagnaient à être diffusés et utilisés massivement en dépit d’un environnement non entièrement ouvert (contrairement à la GNU GPL qui se refuse à de telles cohabitations). Elle autorise expressément la liaison de logiciels non libres avec la bibliothèque diffusée sous cette licence.

Il s’agissait d’une stratégie destinée à favoriser l’immixtion de logiciels libres stratégiques dans l’écosystème de logiciels propriétaires, dans les situations où les contraintes de la GPL auraient eu des effets trop néfastes sur la diffusion des logiciels en question.

Une nouvelle version de la licence LGPL a été déployée en 1999 : la GNU Lesser General Public licence, avec par la suite une mise à jour effectuée en 2007.

![licencempl](images/mooc-comprendreopensource%20-%20132.jpg)

La licence MPL (Mozilla Public License) a été créée pour la diffusion du code source du navigateur Netscape. La société souhaitait en effet favoriser la création de solutions – mêmes propriétaires – intégrant son logiciel, tout en s’assurant que les modifications apportées resteraient accessibles et disponibles.  

Cette licence raisonne à l’échelle du fichier (et non pas de l’oeuvre tout entière) et demande à ce que tout fichier contenant du code sous licence MPL reste sous cette même licence. Elle a inspiré de nombreuses autres licences, telles les CDDL ou encore EPL, qui la complétèrent et adaptèrent quelques-unes de ses clauses (notamment le copyleft qui fonctionne par plug-in en matière de licence EPL).

La licence a été mise jour en 2012 et est considérée aujourd’hui comme l’une des licences copyleft les plus robustes et réceptives à l’écosystème industriel.

## 3.8. Open Source et brevets

![osetbrevets](images/mooc-comprendreopensource%20-%20135.jpg)

Le brevet est un titre de propriété industrielle qui confère à son titulaire un monopole d’exploitation - c’est-à-dire un droit d’interdire toute exploitation par des tiers - temporaire (pendant au maximum 20 ans) - sur le territoire d’un État donné. 

Un brevet porte sur une invention brevetable relative à un produit ou à un procédé et sert à contrôler l’utilisation qui sera faite d’une invention. Contrairement au droit d'auteur, il n’est pas possible de “simplement” réécrire la fonctionnalité brevetée.

En France, comme en Europe, les brevets ne peuvent en principe pas porter sur des logiciels. Ce n’est cependant pas le cas aux États-Unis ou au Japon.

![licencegnugpl89](images/mooc-comprendreopensource%20-%20137.jpg)

Les brevets logiciels sont très contestés et font l’objet de nombreux abus, parmi lesquels figurent notamment les patent trolls : il s’agit de sociétés dépourvues de département R et D (Recherche & Développement) qui vont acheter des brevets tiers afin de négocier des licences ou d’agir en justice.

Sous couvert de favoriser la diffusion de l’innovation, elles cherchent uniquement à maximiser le gain relatif à leurs actifs.

![groupementbrevets](images/mooc-comprendreopensource%20-%20138.jpg)

En réaction au système des patent trolls, certains acteurs industriels ont choisi de se regrouper pour former des groupements de brevets à titre défensif. IBM, Google, Red Hat, etc. sont à la tête d'initiatives collectives afin de défendre certains secteurs économiques. 

De cette manière, ils se protègent mutuellement contre toute action en contrefaçon de leurs brevets respectifs et ont parfois vocation à favoriser l’innovation dans un domaine donné (comme en matière d’énergie pour Tesla).

Certains de ces groupements vont plus loin encore dans cette démarche, en promettant aux communautés Open Source de ne pas leur opposer leurs brevets.

Ainsi, L’Open Invention Network (OIN) - consortium composé d’industries de haute technologie - a conduit l’initiative nommée « Linux Defenders » qui a vocation d’aider à protéger les communautés de logiciels Open Source contre les dangers et attaques récurrentes en matière de brevets.

![clausespecifique](images/mooc-comprendreopensource%20-%20139.jpg)

Le détenteur d’un brevet peut autoriser ceux qui le souhaitent à reproduire son invention. Ainsi, il est tout à fait possible pour le titulaire d’un brevet d’accorder une licence à quiconque souhaite l’utiliser dans le cadre d’un projet Open Source.

Certaines licences prévoient ainsi des clauses spécifiques en matière de brevets, comme la licence Apache ou la licence GPL.

En ce sens, la licence GPL version 2 contient une clause implicite empêchant toute société distribuant un logiciel sous cette licence de poursuivre ensuite les utilisateurs subséquents (ses propres utilisateurs, mais également les utilisateurs de ses utilisateurs) pour violation de brevets. La version 3 de la licence GNU GPL va encore plus loin, et inclut une clause explicite qui va étendre cette obligation de protection des brevets.

## 3.9. Open Source et marques

![licencegnugpl89](images/mooc-comprendreopensource%20-%20140.jpg)

Le droit des marques vient protéger un signe distinctif enregistré.

La marque peut prendre plusieurs formes (un mot, un symbole, voire même une forme, une couleur ou un son), mais elle a toujours le même but : être reconnue par le consommateur.

![proprieteintellectuelle](images/mooc-comprendreopensource%20-%20142.jpg)

Pour rappel, le droit de la propriété intellectuelle n’envisage pas un produit commercialisé comme un objet unique, mais comme une multitude d’objets juridiques distincts, protégés chacun par un droit de propriété intellectuelle différent et adapté.

Ainsi, un projet Open Source pourra être protégé à la fois par :
* Le droit d’auteur, pour le code source qui sera donc soumis sous licence OS;
* Le droit des brevets, pour les éventuels brevets qui auraient pu être déposés (même si, on l’a vu, les brevets en matière de logiciels ne sont en principe pas valable en Europe) ;
* La marque qui aura été associée au projet ;
* etc.

Tous ces droits sont totalement indépendants : le code d’un logiciel peut donc être « libre » (placé sous une licence Open Source), mais le titulaire de la marque peut empêcher son utilisation par d’autres, sans que soit remise en cause la qualité du logiciel Open Source. 

![protectionmarques](images/mooc-comprendreopensource%20-%20143.jpg)

Les éditeurs diffusant un logiciel en Open Source assurent souvent la protection de leur produit par l’usage complémentaire du droit des marques : le logiciel est librement diffusé sous licence Open Source, mais tout usage de la marque est soumis à autorisation.

L’éditeur est ainsi assuré d’obtenir un contrôle sur l’évolution, la distribution des copies et l’exploitation globale du logiciel.

Dans l’usage de ce droit, il est possible de distinguer les sociétés commerciales, qui ont tendance à en user de façon très agressive au point parfois de réduire les libertés concédées par la licence Open Source sur le produit, et les projets communautaires qui l'utilisent davantage en termes de protection qu'en termes de commercialisation (MySQL, Red Hat, Firefox, etc.).

![fork](images/mooc-comprendreopensource%20-%20144.jpg)

Néanmoins, ces sociétés doivent faire attention à ne pas se mettre la communauté à dos. En effet, l'Open Source est basé sur le choix de la contribution de la communauté, mais dans cette situation on l'empêche de contribuer amenant parfois cette dernière à se détourner de l'éditeur initial. 

De ce fait, la communauté peut créer ce que l’on appelle un « fork », c’est-à-dire un projet parallèle : la communauté va s’émanciper en cessant toute allusion à la marque et en reprenant à son compte les logiciels diffusés sous licence Open Source. Ceci peut ensuite avoir de lourdes conséquences pour le projet qui perdra un grand nombre de ses contributeurs.

# 4. Organisation des projets et de l'écosystème

## 4.1 Organisation, gouvernance de projets Open Source

![organisationetgouvernance](images/mooc-comprendreopensource%20-%20147.jpg)

La très grande diversité des projets diffusés sous licences libres fait apparaître une variété de modes de fonctionnement et d’organisation.

Il existe cependant des motifs largement répandus et des critères simples permettant de distinguer les projets. 
Le premier critère pour appréhender le fonctionnement d’un projet Open Source est son type de gouvernance et on en distingue deux catégories principales : les projets avec une gouvernance de type éditeur et ceux avec une gouvernance communautaire. 

![gouvernanceediteur](images/mooc-comprendreopensource%20-%20149.jpg)

Dans le cas d’une gouvernance éditeur, c’est une entité unique qui décide des orientations du projet, en termes de roadmap, d’intégration ou de contribution externe. La traduction dans les faits de ce type de gouvernance varie énormément en fonction des projets. 

Du point de vue de l’utilisateur, une telle gouvernance offre l’avantage de la simplicité (l'acquisition et le support du produit s’intègrent de façon transparente dans les processus existants), mais peut limiter certains aspects propres à l’Open Source, que l’on retrouve dans les projets communautaires.

![gouvernancecommunautaire](images/mooc-comprendreopensource%20-%20150.jpg)

La gouvernance de type communautaire est celle qui vient le plus naturellement à l’esprit quand on parle de projets Open Source : elle peut être assurée au sein d’une entité à but non lucratif (Dans le contexte de l’Open Source, on parle généralement de « fondations »), ou être informelle, même pour des projets de grande envergure (c’est par exemple le cas de PostgreSQL ou Samba).

Les décisions sont le plus souvent issues de consensus ou de votes et reposent plutôt sur une idée de méritocratie, même si ce sujet fait débat. 

Il faut noter que les positions de responsabilité ne sont pas souvent l’enjeu de rivalités fortes, car ce sont plus des positions de devoir que des positions de pouvoir. 

On verra par exemple, le faible nombre de candidats au poste de leader pour un projet majeur comme le projet Debian. 

Il y a généralement une approche plus horizontale que hiérarchique dans la prise de décision, en revanche l’organisation fonctionnelle du projet peut adopter un modèle hiérarchique pour des considérations techniques (le noyau Linux a une organisation pyramidale très structurée).

![typescontribution](images/mooc-comprendreopensource%20-%20151.jpg)

L’organisation d’un projet se fait également selon les différents types de contribution. Le principe de l’Open source est de permettre à plusieurs acteurs différents de collaborer à un même  projet. Ces contributions peuvent s’effectuer à des niveaux très divers : code, rapport de bugs, documentation, localisation, support entre utilisateurs, etc.

Les utilisateurs simples font partie de la communauté. Ce sentiment d’appartenance permet d'augmenter la motivation à s’impliquer davantage, cela étant facilité par la gradation possible. 

![acteurscommunautaires](images/mooc-comprendreopensource%20-%20153.jpg)

On distingue souvent différents niveaux d’implication dans le projet : on parlait initialement de « core contributeurs » faisant référence à une implication sur le coeur du code. On parle désormais davantage de mainteneurs, ce qui correspond plus à la réalité. En effet, ce qui compte dans l’implication, c’est surtout l’engagement dans la durée. 

![gradedimplication](images/mooc-comprendreopensource%20-%20154.jpg)

L’implication dans le projet se traduit généralement par des droits différents de modification : en effet, si tout le monde peut proposer des modifications leur intégration est forcément soumise à un certain nombre de contrôles.

Pour conclure, les mécanismes d’attributions de ces droits dépendent fortement du type de gouvernance du projet.

Les communautés, c’est comme les antibiotiques, ce n’est pas automatique.

## 4.2 Les forks : entre opportunité et menace

![forkopportuniteetmenace](images/mooc-comprendreopensource%20-%20155.jpg)

Que la gouvernance soit celle d'un éditeur, ou communautaire, le caractère libre du code permet, en cas de désaccord profond avec la direction prise par le projet et la communauté, de créer une branche divergente : c’est ce que l’on appelle plus communément un fork.

![effetfork](images/mooc-comprendreopensource%20-%20156.jpg)

Cette possibilité représente à la fois un risque, car cela divise les ressources, mais c’est aussi une garantie d’indépendance voire une opportunité pour les contributeurs ou utilisateurs finaux qui ne trouvent pas d’autres issues, renforçant ainsi leur confiance dans le projet et leur implication.

![exemplefork](images/mooc-comprendreopensource%20-%20158.jpg)

Par exemple, suite au rachat de Sun par Oracle, nous avons pu assister au fork réussi du projet OpenOffice.org par la communauté sous le nom LibreOffice, ou encore le fork de MySQL par MariaDB. C'est également le cas du serveur d’intégration continue Hudson, forké en Jenkins, logiciel aujourd'hui massivement utilisé.

On retrouve également le logiciel de dessin vectoriel Inkscape, qui est un fork de Sodipodi suite à des difficultés à intégrer de nouvelles fonctionnalités dans le projet initial : in fine, Inkscape a connu un fort développement alors que la branche initiale s’est éteinte. Autrement dit, lorsque le projet n'est pas en capacité d'aller dans la direction voulue par la communauté, celle-ci s'en empare et l'amène où elle le souhaite. 

D’autres forks peuvent avoir moins de succès (comme libAv, fork de FFMPEG) :  dans ce cas cependant, le fork a eu un effet bénéfique sur le projet original en l’incitant à évoluer organisationnellement. 

![openam](images/mooc-comprendreopensource%20-%20159.jpg)

Lorsque l’éditeur d’un logiciel Open Source “dépasse” les contraintes jugées acceptables par la communauté (composée notamment de ses clients) des projets de forks peuvent ainsi être lancés à l’initiative de ces derniers pour reprendre le contrôle sur ce projet dans lequel ils ont investi.

C’est ainsi qu’un Fork du projet OpenAM a récemment été initié par les partenaires et clients de la Société Forgerock. On peut voir sur le site Time For A Fork l'intention et les réalisations de ces divers acteurs au profit d'une version entièrement OS du projet.

![forketmarque](images/mooc-comprendreopensource%20-%20160.jpg)

La gestion de la marque est en revanche un point particulièrement sensible : c’est cette notion qui a maintenu OpenOffice.org à flot, mais qui a également causé certains dégâts puisque Libre Office démontre un peu de mal à retrouver les connaissances et l'image dont bénéficiait son prédécesseur. Cela joue surtout pour les projets grand public. Par contraste, tout le monde a oublié Hudson et utilise maintenant Jenkins. 

## 4.3 Typologie d’acteurs de l’écosystème

![acteursecosysteme](images/mooc-comprendreopensource%20-%20161.jpg)

Très hétérogène, l'écosystème de l’Open Source se compose de nombreux acteurs aux rôles variés. Les conséquences d’une telle pluralité sont positives : l’Open Source appartient à tout le monde et CELA LAISSE LA PLACE À L'INITIATIVE. 

Il est ainsi possible d’évoquer les communautés, l'industrie du Logiciel Libre (au sens large) qui comprend Les bénéficiaires finaux (en tant que clients et prescripteurs de spécifications), ou encore les fondations.

![acteursprives](images/mooc-comprendreopensource%20-%20162.jpg)

Les acteurs privés 

Les éditeurs Open Source sont des sociétés qui investissent dans le développement d’un produit logiciel qu’elles distribuent tout ou en partie sous une licence Open Source. On peut par exemple citer :  
* RedHat (distribue une version GNU Linux),
* la PME française  SensioLabs (à l'origine du framework Symfony), 
* Alfresco, qui édite un système de gestion de contenus

On retrouve ensuite une autre catégorie d'entreprise : les "Pure Players", sociétés spécialisées dans un domaine particulier et contribuant à des projets Open Source communautaires : 
* Collabora (UK, contributeurs à LibreOffice et LibreOffice Online), 
* OSlandia (FR, contributeurs aux projets QGis et PostGIS), 
* VideoLAbs (FR, contributeurs au logiciel VLC)

Pour compléter la liste des acteurs privés, on peut mentionner des sociétés de service spécialisées en Logiciel Libre. Elles jouent principalement un rôle d’intégrateur et d'interface avec le client et offrent aussi des guichets uniques de support sur de larges ensembles de composants Open Source. C'est notamment le cas de : 
* Smile , Alter Way, Savoir Faire Linux, …     

Des entreprise de Service Numérique généralistes jouent un rôle similaire, et éventuellement en partenariat avec des acteurs plus spécialisés : 
* Cap Gemini, Atos, CGI, etc. : 

Enfin, on retrouve des entreprises utilisatrices de logiciels libres dans tous les secteurs d’activité : le modèle de ce type de logiciel permet une fluidité entre le rôle d’utilisateur et de contributeur. On constate également que de plus en plus d’entreprises s’impliquent activement dans cette logique, soit pour contribuer à des projets Open Source, soit pour publier leurs propres développements. Cette tendance a commencé par les constructeurs de matériel avant de s'étendre à d'autres domaines. En France la Société Générale, la SNCF, EDF sont particulièrement actifs dans le domaine de l'OS.

![forgespubliques](images/mooc-comprendreopensource%20-%20163.jpg)

Forges publiques

Une autre composante importante dans le domaine de l' Open Source est constituée des éditeurs de forges. Ce sont des plateformes mises à disposition des contributeurs afin de développer leur projet de manière collaborative. La plupart sont des acteurs privés, mais leur position les oblige à avoir une certaine loyauté dans leurs pratiques (c’est cette absence de loyauté qui a été reprochée à sourceforge).

Leur pérennité n’est pas forcément assurée (tel Google Code qui a fermé récemment) ce qui rend d’autant plus intéressant des projets tels que Gitlab (qui propose une version Open Source de son logiciel sur sa plateforme de forge publique).

![softwareheritage](images/mooc-comprendreopensource%20-%20164.jpg)

À noter une nouvelle fois : le projet Software Heritage permet d’assurer une pérennité par l’archivage de tous les codes sources passés, présents et futurs transitant sur ces plateformes. C'est donc une mémoire colossale maintenue par un acteur français et soutenue par différents autres acteurs dans le monde entier. 

![fondations](images/mooc-comprendreopensource%20-%20165.jpg)

Les acteurs à but non lucratif sont aussi très nombreux dans le paysage de l’Open Source, et jouent des rôles particulièrement structurants pour l’écosystème.

Ils prennent une dimension variable selon qu’ils portent sur un projet, qu’ils soient portés par une fondation ou encore qu’ils fassent partie d’un ensemble plus important soutenu par un consortium ou une fondation “parapluie”.

Leur rôle est tel qu’une vidéo propre leur est consacrée. 

## 4.4 Le rôle particulier des NGOs / à but non lucratif

![ongs](images/mooc-comprendreopensource%20-%20166.jpg)

L’écosystème de l’Open Source tire sa richesse de la diversité des acteurs qui la composent, et des modes d’organisation et de collaboration innovants qu’ils ont mis en place.

Parmi eux, les acteurs à but non lucratif jouent un rôle fondamental. Il en existe plusieurs types, qui répondent à des besoins différents et dont les leviers d’action varient. 

![lesfondations](images/mooc-comprendreopensource%20-%20167.jpg)

Les fondations sont des acteurs clés de l’écosystème, garants du développement et de la pérennité de nombreux projets open source.

Elles interviennent à plusieurs niveaux, tout en ayant des spécificités propres en fonction des contextes et objectifs des projets qu’elles portent.

Parmi leurs actions, elles fournissent la puissance logistique, l’infrastructure et les outils nécessaires aux projets open source pour aboutir à la mise à disposition d’un produit fini. Inclusives, elles permettent ainsi à des acteurs de natures différentes de travailler ensemble.

De plus, elles coordonnent et animent le fonctionnement de la communauté, en mettant à disposition des membres des outils de discussion et de travail collaboratif, et en formalisant des règles de gouvernance collaboratives et transparentes encadrant les relations entre les parties prenantes. 

Elles peuvent également sécuriser juridiquement et protéger le travail des contributeurs aux projets qu’elles hébergent, permettre de recevoir des dons et des ressources de personnes physiques et morales, ou encore, défendre les marques des projets. 

![exemplefondation](images/mooc-comprendreopensource%20-%20168.jpg)

Qui sont ces fondations ? Certaines fondations ou associations portent plusieurs projets logiciels en parallèle, comme la fondation Apache (qui développe des logiciels open source sous la licence Apache), la fondation Linux (qui a pour but de protéger et standardiser Linux), ou encore la fondation Eclipse (qui porte de multiples projets OS), etc. 

D’autres fondations sont consacrées à un projet logiciel spécifique, ou à un ensemble restreint de logiciels. Ainsi, un projet comme Libre Office est géré par The Document Foundation, tout comme Mozilla, Open Stack, Gnome ou encore Drupal qui bénéficient chacun de leur propre fondation dédiée. 

![associationsetfondations](images/mooc-comprendreopensource%20-%20170.jpg)

D’autres acteurs à but non lucratif – des associations et des fondations principalement – ont pour principal objectif de promouvoir le logiciel libre : elles définissent les principaux concepts qui fondent le logiciel libre et l’open source. 

→ Parmi les plus anciennes et influentes figurent la Free Software Foundation, l’Open Source Initiative et le Software Freedom Law Center. 

![autresassociationsetfondations](images/mooc-comprendreopensource%20-%20171.jpg)

D’autres organisations vont plus loin, en veillant activement à l’application de ces principes. Ces organisations, comme la Software Freedom Conservancy, ou le Software in the Public Interest, mènent des actions en justice pour défendre les utilisateurs dont les libertés n’auraient pas été respectées, ou bien les contributeurs dont les licences de projets n'auraient pas été honorées.

![evolutioncadrejuridique](images/mooc-comprendreopensource%20-%20172.jpg)

Certains acteurs – principalement des associations – ont pour mission de faire évoluer les cadres juridiques et réglementaires qui favorisent l’usage des logiciels libres. En France, des associations comme l’April, Framasoft, l’AFUL sont particulièrement actives, tandis qu’au niveau européen, on remarque par exemple l’action de la Free Software Foundation Europe ayant également pour vocation de soutenir l'évolution du logiciel libre. 

En parallèle, des associations professionnelles ont pour objectif de structurer la filière du logiciel libre comme le Conseil National du Logiciel Libre (CNLL) en France, l’Open Source Business Alliance qui est son homologue allemand, ou encore l’Adullact en France qui agit plus au niveau des collectivités locales et des administrations, notamment en matière de mutualisation des ressources. 

![consortiums](images/mooc-comprendreopensource%20-%20173.jpg)

Enfin, de plus en plus, des acteurs industriels - parfois concurrents - identifient l’intérêt de mutualiser, de mettre en commun leurs ressources et compétences afin d’accélérer collectivement le développement de projets Open Source.

Certains font le choix de créer une structure à but non lucratif pour porter et organiser leur action, comme l’alliance GENIVI dans le secteur des transports qui a pour objet de promouvoir l’adoption d’une plateforme d’infotainment (ou d'info divertissement) pour véhicule. C'est aussi le choix de OW2, association indépendante dédiée au développement d’une base de logiciels d’infrastructure, jouant un rôle similaire à la fondation Eclipse. 

## 4.5 Modèles économiques

![modeleseconomiques](images/mooc-comprendreopensource%20-%20174.jpg)

On l’a vu dans les chapitres précédents, les projets Open Source peuvent correspondre à des réalités très hétérogènes en termes d’organisation et de modèle économique qui les sous-tendent, les renforcent et qui présentent in fine une grande variété. 

![ventelicence](images/mooc-comprendreopensource%20-%20175.jpg)

Pour les projets avec une gouvernance éditeur, une source de revenus peut être la vente de licences propriétaires. Cette vente peut porter :
* soit sur une version plus évoluée que le produit Open Source distribué (on oppose généralement une version “community” à une version “entreprise”),
* soit pour des produits complémentaires au produit Open Source (sous forme de module ou de greffon),
* soit pour le même produit que le produit Open Source, mais dégagé de certaines obligations liées à sa licence Open Source (comme la clause de Copyleft, exemple assez représentatif de MySQL).

Certains éditeurs peuvent également vendre des souscriptions ou des abonnements : du point de vue de l’utilisateur, cela ressemble beaucoup à la vente de licence, en revanche ce que paie l’utilisateur n’est pas lié à l’usage du code en tant que tel, mais pour son usage dans un contexte facilité : à la fois en termes de certifications (usage de la marque de l’éditeur) et technique (facilité de mise à jour, etc.). L’exemple le plus connu est ici celui de l’entreprise Red Hat.

Le modèle économique le plus volontiers associé à l’Open Source est la vente de service autour du logiciel : 
* Cela peut se faire de façon classique : l’entreprise vend des prestations basées sur son expertise sur le logiciel : intégration, conseil, formation, support, etc.
* Plus récemment, cela peut aussi être le développement de services sous forme de plateforme SaaS basée sous le logiciel Open Source. Le client conserve la possibilité d’installer et d’administrer le logiciel sur son parc, mais l’utilisation directe du service hébergé chez un tiers peut être plus rentable pour lui. C’est notamment le cas de l’offre de service de la Société Aquia, à l’origine du célèbre logiciel Drupal.

Il est également fréquent que des acteurs financent le développement d’un logiciel Open Source, car celui-ci est nécessaire à leur activité métier, sans constituer un élément différenciateur en lui-même. La mutualisation que permet l’Open Source rend cette stratégie plus rentable qu’un développement interne. 

D'autre part, il peut s’agir aussi bien de vente de matériel ou encore de services reposants sur du logiciel Open Source. De ce fait, les constructeurs de matériel (IBM, Intel, Huawei, etc.) et les géants du Web reposent tous énormément sur les logiciels Open Source, et sont des contributeurs importants à des projets communautaires majeurs ou eux même à l’origine de projets (Linux, Android, etc.). 

En termes de financement alternatif, certains projets Open Source s’appuient sur des logiques de mécénat / donation ; ou encore de publicité.

Certains projets Open Source communautaires font appel aux dons de leurs utilisateurs pour financer leurs développements, soit de façon ponctuelle, soit de façon permanente. Pour que ce modèle puisse fonctionner de façon permanente, il est nécessaire que le projet soit dynamique et proche de ses utilisateurs. À noter que des projets comme Libre Office bénéficient à la fois d'un mécénat de particuliers et d'entreprises, voire d'États, bien que les dons individuels soient plus importants. 

D'autres projets communautaires se financent uniquement sur la publicité : le logiciel libre étant susceptible de diffusion large et rapide il constitue un bon support pour embarquer des messages ou des services. Mozilla, par exemple, a des accords avec les moteurs de recherche lui ayant permis de financer une grande part du développement de Firefox. 

![oscommunautaire](images/mooc-comprendreopensource%20-%20176.jpg)

Trouver son modèle économique libre n’est pas donc pas nécessairement aisé. Cependant, il faut noter que parfois la viabilité économique d’un projet passe par une forme Open Source communautaire et non par une entreprise privée, qu’elle soit propriétaire ou Open Source. 

Par exemple, le projet Blender initialement propriétaire n’a trouvé sa viabilité économique qu’en passant sous forme de projet communautaire grâce à une implication totale dans cette direction : productions crowdfundées disponibles sous licences libres, etc. 

En d'autres termes, cela peut être sous une multitude de formes se trouvant entre l'économie sociale et solidaire et des aspects plus associatifs.   

![pasdautreredhat](images/mooc-comprendreopensource%20-%20177.jpg)

 En conclusion :  

« Il n’y aura pas d’autre Red Hat ». C'est un sujet fréquemment évoqué. Certains exemples de rachats font rêver (comme celui de MySQL), et le succès de RedHat aussi, mais c'est une réussite liée à un contexte historique particulier et aujourd'hui l'Open Source tend plutôt à une répartition de la valeur au profit d'une multitude d'acteurs. 

L’Open Source n’est pas adapté à créer une captation vers un acteur unique. En effet, une grande partie de la valeur créée est diffusée dans l’ensemble de l’économie numérique, et non concentrée dans un unique acteur. C’est cet aspect diffus qui rend l’Open Source si important d’un point de vue sociétal global.

Red Hat est souvent cité comme exemple de réussite, comme modèle, que l’on cherche à imiter ou à décliner à d’autres domaines que Linux. On a vu plusieurs sociétés se revendiquer « le RedHat de » (Open Stack, etc.) et le succès de RedHat est certes bien réel (l’entreprise réalise un CA de plus d’1 ,Md de $), néanmoins ce modèle s’appuie sur un contexte historique et technologique particulier et les nouveaux modèles amènent à favoriser des structures plus petites, agiles et complémentaires entre elles.

## 4.6 Développements durables? 

![developpementdurable](images/mooc-comprendreopensource%20-%20178.jpg)

Une stabilité pas encore atteinte

Comme expliqué en introduction, l’Open Source est désormais omniprésent, incontournable, ancré dans le paysage. Cela donne l’impression trompeuse d’une situation de stabilité, alors que le phénomène est encore jeune, et se renouvelle constamment, car le contexte technologique est très mouvant. 

Au cours des dernières années, plusieurs incidents sont venus rappeler que tout n’était pas si stable. 

![openssl](images/mooc-comprendreopensource%20-%20179.jpg)

L’exemple le plus marquant concerne OpenSSL, une bibliothèque logicielle permettant de sécuriser les échanges sur Internet. Cette brique fondamentale est utilisée par des milliers de produits, des millions de site Web et d’applications critiques, notamment dans le domaine bancaire et financier.

Cette bibliothèque a été affectée par une faille de sécurité majeure, surnommée Heartbleed, compromettant la sécurité de millions de transactions dans le monde entier. 

L’analyse des causes de cette faille a mis en évidence les moyens dérisoires dont disposait le projet et l’absence de contribution des utilisateurs.

![coreinfrastructureinitiative](images/mooc-comprendreopensource%20-%20180.jpg)

En réponse à cet incident, un ensemble d’entreprises grandes consommatrices de logiciels libres (Google, Intel, Bloomberg, etc.) ont lancé la Core Infrastructure Initiative, hébergée par la Linux Foundation afin de tirer les leçons de cet incident et de donner des moyens au projet OpenSSL, mais aussi à d’autres projets dans une situation similaire (OpenSSH, GnuPG, Linux Kernel Self Protection Project, etc.).

![busfactor](images/mooc-comprendreopensource%20-%20181.jpg)

Le problème de manque de moyens affecte un périmètre bien plus large de logiciels et renvoie à la notion de “Le bus factor” aussi dit de « résistance à l’écrasement ». C’est-à-dire que la pérennité du projet repose sur un nombre limité de personnes et qu’un “simple” accident serait susceptible de compromettre gravement le projet. 

Ce phénomène se retrouve autant au sein de projets communautaires que de projets industriels, et relève surtout de la question de la gouvernance.

À cela s’ajoute que la structure actuelle des développements fait que de nombreux projets dépendent de très nombreux « petits » projets. De fait, la santé des petits projets revêt aussi une importante majeure (c’est un cas rencontré par exemple dans le cadre de leftpad). 

![cerclevirtueux](images/mooc-comprendreopensource%20-%20182.jpg)

Ces éléments renforcent la prise de conscience progressive d’une nécessité de contribution directe ou indirecte par les utilisateurs finaux des projets (en choisissant ses prestataires selon leur participation effective aux communautés des projets Open Source). 

Outre la pérennisation des outils qu’elles utilisent, ce type de contribution porte de nombreux avantages pour les entreprises qui en tirent une meilleure maîtrise et, plus généralement, tous les bénéfices de l’Open Source.

# 5. Impacts de l’Open Source sur l’organisation des entreprises

## 5.1- Transformation numérique et modèles ouverts

![transformationnumérique](images/mooc-comprendreopensource%20-%20186.jpg)

Aujourd'hui toutes les entreprises et organisations parlent de transformation numérique, notion leur permettant de mettre en avant leur valeur ajoutée. Autrement dit, cela leur permet de s'appuyer sur des ressources à la fois adaptées à leur métier, mais également maîtrisées. 

![empoweringopeninnovation](images/mooc-comprendreopensource%20-%20187.jpg)

Bien que l'intégralité des grands groupes considère avoir initié leur transition digitale, on se rend compte que la plupart n'en sont qu'au démarrage. Cette transformation étant à la fois une opportunité et une menace pour les entreprises, une incapacité de réagir avec rapidité face à cette évolution peut leur faire perdre l'avantage par rapport à leurs concurrents. 

De plus en plus d'entreprises sont convaincues que l'innovation ouverte, fondée sur des mécanismes d'ouverture et de collaboration, doit jouer un rôle prépondérant dans l'accélération de leur transformation numérique. 

En effet, le numérique permet aux entreprises de parler un langage commun qui les rend à même de faciliter leur mutualisation et de s'impliquer dans les projets qu'ils utilisent au quotidien en tant que contributeurs ou prescripteurs de ces logiciels. Sur le long terme, l'enjeu consistera pour tous les acteurs industriels et publics de miser ensemble sur des ressources leur permettant d'innover et pérenniser leur évolution digitale. 

![societegenerale](images/mooc-comprendreopensource%20-%20188.jpg)

Plusieurs grands groupes ont notamment fait le choix stratégique d'opter pour l'Open Source comme levier de leur transformation numérique. 

Par exemple, c'est le cas de la Société Générale par Françoise Mercadal-Delasalles, Directrice des Ressources et de l'Innovation : "L'Open source est un formidable accélérateur de notre stratégie de digitalisation de la banque".

![voyagesncf](images/mooc-comprendreopensource%20-%20189.jpg)

Mais également celui du groupe Voyage SNCF Technologies par Gilles de Richemond, CEO : "Chez VSC Technologies, l'utilisation des produits Open Source fait partie de notre quotidien depuis l'origine et pour la grande majorité de nos besoins. Notre transformation ces quatre dernières années n'aurait jamais pu avoir lieu en dehors du monde open source".

Autrement dit, l'Open Source fait partie de leur quotidien.

![openlawetfabriquedesmobilites](images/mooc-comprendreopensource%20-%20190.jpg)

Ensuite, on retrouve deux écosystèmes ayant vocation d'accompagner la transformation numérique de leurs acteurs : Open Law et La Fabrique des Mobilités. Ainsi, ces structures associatives permettent de favoriser le développement numérique au sein d'un écosystème en mettant à disposition des ressources ouvertes.  

## 5.2- SI : Open Source et qualité juridique

![osetqualitejuridique](images/mooc-comprendreopensource%20-%20191.jpg)

L’Open Source a grandement simplifié la vie à des millions de développeurs et la réutilisation de code Open Source dans les développements est désormais une pratique quotidienne, quel que soit le secteur. La conséquence est néanmoins une vigilance accrue en interne quant à l’usage de ces composants afin de s’assurer d’une conformité permanente vis-à-vis des licences des composants Open Source utilisés.

![gérerlecodetiers](images/mooc-comprendreopensource%20-%20192.jpg)

Cela a induit des évolutions techniques pour automatiser cette pratique et notamment la généralisation des outils de gestion des dépendances dans les différents langages (maven pour java, composer pour PHP, PIP pour Python, npm/yarn pour JavaScript, Cargo pour Rust, etc.).

Cela a aussi entraîné des évolutions organisationnelles pour gérer la présence massive de code tiers dans les développements réalisés au sein des organisations, en particulier du point de vue de la sécurité et de la légalité.

Comme on l'a vu précédemment, les licences Open Source confèrent des droits, mais également des devoirs à respecter au risque de graves conséquences.

![processusdeconformite](images/mooc-comprendreopensource%20-%20193.jpg)

Il est ainsi impératif d'intégrer la conformité à ces licences dans le processus des différentes étapes de la chaîne de développement.

On peut distinguer trois étapes principales dans ces processus de conformité :
* Identification des composants
* Validation des licences associées
* Exécution des obligations

La première étape consiste à déterminer les composants Open Source entrant dans la composition d'un développement ainsi que la ou les licences qui leur sont associées. 

Si une politique spécifique est déjà en place au moment du développement, cela peut se faire de façon déclarative. Dans le cas de l'usage d'outils de gestion de dépendance, ceux de premier niveau sont facilement connus puisqu'ils sont déclarés explicitement, et l'outil permet de calculer automatiquement celles des niveaux suivants (c'est-à-dire les dépendances des dépendances, etc.). 

Lorsque la phase d'identification se fait a posteriori, par exemple lorsque le code développé est récupéré d'une entité tierce ou en l'absence d'une politique interne de gestion des composants open source, elle peut être facilitée par certains outils : outil recherchant des textes de licences ou de mentions de droits d'auteur (du type Fossology), ou comparant le code analysé à une base d'empreintes des projets Open Source publics (Black Duck, Flexera, etc.). 

Une fois l'ensemble des composants tiers et leurs licences respectives identifiées, il s'agit de valider leur compatibilité entre eux (certaines licences pouvant avoir des exigences contradictoires) et avec les objectifs de l'entreprise pour son développement : l'aspect le plus impactant est généralement le caractère copyleft de certaines licences parce qu'il a de fortes implications sur les modèles économiques possibles.

Une fois la liste des composants établis validée (on parle généralement de Bill of Material), il s'agit d'exécuter les obligations découlant des licences impliquées, que ce soit en matière de mentions de paternité (dans la documentation ou l'application elle-même) ou de mise à disposition de code source.

Encore une fois, cet effort contribue globalement à une meilleure qualité juridique et renforce l’organisation de l’entreprise.

## 5.3- Urbanisation et modèles ouverts : Interopérabilité et formats ouverts

![urbanisationetmodelesouverts](images/mooc-comprendreopensource%20-%20195.jpg)

L'Open Source a eu un impact important sur le système d'information en fournissant un nouveau panel de solutions prêt à l'emploi, mais a aussi permis de le concevoir différemment (faire interagir différentes composantes intelligentes entre elles).

Les notions d'interopérabilité, d'urbanisation des systèmes d'information, de format ouvert, de gestion de la donnée sont entièrement disjointes des notions de logiciels libres et open source, bien que fortement liées dans un contexte historique et culturel. 

![urbanisation](images/mooc-comprendreopensource%20-%20196.jpg)

On observe une convergence naturelle entre les objectifs de l'interopérabilité et ceux de l'open source dans la réduction de la dépendance à un fournisseur particulier, ce que l'on appelle classiquement "vendor lock-in". Avec la sophistication croissante des systèmes d'information, les interactions avec les différentes composantes se sont développées. De plus, du point de vue des vendeurs de solutions la capacité qu'avaient leurs produits à n'interagir qu'entre eux était le moyen de conserver une clientèle captive et donc d'éliminer la concurrence sans avoir à rivaliser directement avec celle-ci. 

Ces stratégies d'enfermement sont de fait opposées à une approche ouverte aussi bien au niveau philosophique (liberté des utilisateurs) que technique (ouverture du code). De plus, les solutions open source se sont développées en favorisant au maximum les interactions avec les solutions tierces. Elles sont également apparues comme solutions alternatives sur les marchés verrouillés par des acteurs dominants. Dans ce cadre, l'interopérabilité est un facteur crucial pour rendre leur adoption possible. 

L'open source est venue apporter des cas d'application concrets aux principes théoriques de l'urbanisation des systèmes d'information, en proposant notamment des solutions techniquement et économiquement plus intéressantes. 

Cela a permis de démontrer l'importance de concevoir des systèmes d'information correctement urbanisés, c'est-à-dire composés de briques fonctionnant harmonieusement entre elles, tout en étant indépendantes les unes des autres. 

![rgi](images/mooc-comprendreopensource%20-%20197.jpg)

C'est d'ailleurs sur ce fondement que la gendarmerie a graduellement mis en oeuvre des solutions open source au sein de son système d'information. En termes d'urbanisation et d'interopérabilité ces avancées ne profitent pas seulement aux solutions open sources, mais bien à toutes les solutions inscrites dans une pratique de standard ouvert, étant la plupart du temps des acteurs entrants innovants. 

## 5.4- Innersourcing

![innersourcing](images/mooc-comprendreopensource%20-%20198.jpg)

L'inner sourcing reprend les points clés des communautés open source tant dans sa définition que dans sa méthodologie, afin de les appliquer en interne à l'entreprise ou à l'organisation.

![timoreilly](images/mooc-comprendreopensource%20-%20201.jpg)
Le terme Inner Source a été inventé par Tim O’Reilly en 2001, pour désigner l’utilisation des techniques et mécanismes de l’Open Source au sein de l’organisation. 

À travers la culture, les pratiques et la méthodologie telles qu'utilisés et appliqués dans les projets open source, elle s'appuie sur la collaboration, la transparence, le mérite, et l'autonomie au sein des entreprises. Ce dernier point, étant un élément crucial, rend l'application de l'inner sourcing parfois compliquée tant elle demande une réforme complète de la culture et de la pensée du modèle de l'entreprise.

![7pratiquesindispensables](images/mooc-comprendreopensource%20-%20202.jpg)

Cette collaboration est rendue possible par la transparence des processus, ceux-ci étant accessibles à l'ensemble des participants. Toutes les décisions, les discussions, ainsi que les étapes de la vie du projet doivent être mises à disposition de tous. Cela permet à ceux qui prennent le projet en cours d'avoir accès au même niveau d'information que ceux qui sont déjà présents depuis le début.

De même tous les outils doivent être accessibles. Cela permet de maintenir une culture et une co-création entre les équipes laissant place à un terrain fertile pour l'innovation au sein de l'entreprise. Cela favorise également un recrutement qualitatif en étant plus attractif et une fidélisation des talents en interne. Ce partage développe une amélioration de la base de connaissance des individus, une mise en commun des méthodologies et une modélisation des processus à travers les équipes ainsi qu'un "brain-storming" permanent appelant à une plus grande créativité.

Enfin, l'autonomisation des acteurs permet une meilleure priorisation des travaux par les équipes, le développeur travaille sur ce qu'il préfère, cela encourage la méritocratie (c.-à-d. une reconnaissance par rapport aux contributions réelles, les tâches de gestion s'en trouvent donc allégées). Cette responsabilisation rend l'entreprise attractive pour les employés présents et futurs. Prendre des responsabilités, c'est s'investir, rechercher les meilleures opportunités, être créatif, performant et se sentir plus en prise avec la tâche à accomplir, la rendant ainsi plus intéressante.

![bitergia](images/mooc-comprendreopensource%20-%20203.jpg)

L'outil open source Bitergia permet d'analyser la communauté et les contributeurs rattachés à une entreprise. Il permet en effet de visualiser rapidement la localisation de ces contributeurs, le type de contribution apporté et savoir si les modèles open source sont utilisés.

Très proches du modèle open source et largement présentes dans l'innersourcing, les méthodes agiles représentent des pratiques de gouvernance de projet, se veulent plus pragmatiques que les méthodes traditionnelles et proviennent du manifeste agile rédigé en 2001.

Elles impliquent au maximum tous les acteurs autour du projet, y compris le client, et permettent une grande réactivité à ses demandes.

Les méthodes agiles reposent sur un cycle de développement (un cycle comporte un petit nombre de phases) et les cycles sont répétés autant de fois que l’on en a besoin, au fur et à mesure, afin de réadapter le projet en fonction de son avancement.

Le manifeste Agile pose 4 principes qui ne vont pas sans rappeler ceux de l’Open source :
* Les individus et leurs interactions, plus que les processus et les outils
* Des logiciels opérationnels, plus qu’une documentation exhaustive
* La collaboration avec les clients, plus que la négociation contractuelle
* L’adaptation au changement, plus que le suivi d’un plan

Pour ce qui est du lean, cette méthode vise à minimiser le gaspillage, avec une gestion “plus juste” des ressources.                   
À cet effet, le lean se fixe comme objectif d’éradiquer trois « démons » de l’organisation du travail :
* le Muda : tout ce qui est sans valeur     
* le Muri : l’excès, la surcharge de travail engendrée par des processus non adaptés :
* le Mura : la variabilité, ou l’irrégularité

![lean](images/mooc-comprendreopensource%20-%20205.jpg)

Un des gaspillages à éradiquer dans le lean est la non-utilisation des ressources intellectuelles du personnel, cela pouvant être évité en appliquant les principes de l’Open source : chaque personne souhaitant participer au projet est en mesure de le faire et n’est pas bridée pour des raisons irrationnelles.

L’éthique des méthodes agiles et du lean, et celle de l’Open source sont relativement similaires, on y retrouve d’ailleurs la communication et la collaboration comme clef de réussite du projet.     

La principale source de similarité entre ces méthodes est l’accent qu’elles mettent sur le partage et l’évolution continuelle du projet.
